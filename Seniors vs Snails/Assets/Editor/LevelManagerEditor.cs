﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor {

    [SerializeField]
    private LevelManager levelMan
    {
        get { return target as LevelManager; }
    }

    public override void OnInspectorGUI() {
        
        levelMan.areaNumber = EditorGUILayout.IntField("Level Number", levelMan.areaNumber);
        levelMan.areaMoneyReward = EditorGUILayout.IntField("Reward", levelMan.areaMoneyReward);
        EditorGUILayout.Space();

        levelMan.unlimitedSnails = EditorGUILayout.Toggle("Unlimited Snails (Testing)", levelMan.unlimitedSnails);
        levelMan.spawningSnailsOnStart = EditorGUILayout.Toggle("Spawning On Start", levelMan.spawningSnailsOnStart);
        EditorGUILayout.Space();


        GUI.backgroundColor = new Color(0.7f, 0.7f, 0.7f);
        // Enemy blocks
        EditorGUILayout.BeginVertical("box");
        GUI.backgroundColor = new Color(0.2f, 0.9f, 0.2f);
        if (GUILayout.Button("Add New Enemy block"))
        {
            if (levelMan.levelSnailBlocks == null)
                levelMan.levelSnailBlocks = new List<EnemyBlock>();

            levelMan.levelSnailBlocks.Add(ScriptableObject.CreateInstance(typeof(EnemyBlock)) as EnemyBlock);

        }
        GUI.backgroundColor = new Color(0.7f, 0.7f, 0.7f);

        int blockNr = 1;
        foreach (EnemyBlock block in levelMan.levelSnailBlocks)
        {
            if (block == null)
            {
                levelMan.levelSnailBlocks.Remove(block);
                break;
            }

            GUI.backgroundColor = new Color(0.7f, 0.7f, 0.7f);
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            GUI.backgroundColor = Color.white;
            GUI.color = Color.white;

            EditorGUILayout.BeginVertical("box");
            GUILayout.Label("Enemy Block Nr: " + blockNr);
            blockNr++;
            EditorGUILayout.Space();

            block.blockType = (EnemyBlock.BlockType)EditorGUILayout.EnumPopup("Block type", block.blockType);
            EditorGUILayout.Space();

            GUI.backgroundColor = new Color(1f, 0.9f, 0.9f);
            EditorGUILayout.BeginVertical("box");

            GUILayout.Label("Enemies:");
            EditorGUILayout.Space();

            // For all enemy snails assigned
            foreach (EnemyBlock.EnemyInstance snail in block.blockSnails) {
                GUI.backgroundColor = Color.white;

                // Change gameObject
                snail.prefab = (GameObject)EditorGUILayout.ObjectField("Snail Prefab", snail.prefab, typeof(GameObject), true);

                EditorGUILayout.BeginHorizontal();

                // Change count
                snail.count = EditorGUILayout.IntField("Count", Mathf.Clamp(snail.count, 0, 100));

                if (GUILayout.Button("Remove enemy"))
                    block.RemoveSnailType(snail);
                
                EditorGUILayout.Space();
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            GameObject empty = null;
            empty = (GameObject)EditorGUILayout.ObjectField("Add Prefab:", empty, typeof(GameObject), true);

            if(empty != null)
                block.AddNewSnailType(empty);

            EditorGUILayout.Space();

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginHorizontal();

            if (block.blockType == EnemyBlock.BlockType.Wave)
            {
                block.spawnInterval =  0f;
            }
            else {
                block.spawnInterval = EditorGUILayout.FloatField("Spawn Interval", Mathf.Clamp(block.spawnInterval, 0f, 20f));
            }
            
            EditorGUILayout.EndHorizontal();

            

            // Areas
            //block.spawnAreas = EditorGUILayout.PropertyField();




            // Remove button
            GUI.backgroundColor = new Color(1, 0.7f, 0.7f);

            if (GUILayout.Button("Remove Block"))
            {
                levelMan.levelSnailBlocks.Remove(block);

            }


            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndVertical();
        }


        EditorGUILayout.EndVertical();

        if (GUI.changed) {
            EditorUtility.SetDirty(levelMan);
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }
}
