﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(MapTileGenerator))]
public class EditorMapTileGenerator : Editor {

    [SerializeField]
    private MapTileGenerator tileGen
    {
        get { return target as MapTileGenerator; }
    }

    public override void OnInspectorGUI() {

        tileGen.tiles_x = EditorGUILayout.IntField("X Tiles", tileGen.tiles_x);
        tileGen.tiles_y = EditorGUILayout.IntField("Y Tiles", tileGen.tiles_y);

        tileGen.tileParent = (Transform)EditorGUILayout.ObjectField("Tile Parent", tileGen.tileParent, typeof(Transform), true);
        tileGen.boundaryParent = (Transform)EditorGUILayout.ObjectField("Boundary Parent", tileGen.boundaryParent, typeof(Transform), true);
        tileGen.staticObjectsParent = (Transform)EditorGUILayout.ObjectField("Static Object Parent", tileGen.staticObjectsParent, typeof(Transform), true);

        tileGen.polynav = (PolyNav2D)EditorGUILayout.ObjectField("PolyNav2D", tileGen.polynav, typeof(PolyNav2D), true);


        EditorGUILayout.Space();



        GUI.backgroundColor = new Color(0.7f, 0.7f, 0.7f);
        // Button
        EditorGUILayout.BeginVertical("box");
        GUI.backgroundColor = new Color(0.6f, 0.6f, 0.9f);
        if (GUILayout.Button("Generate Map"))
        {
            tileGen.CreateMapTiles();

        }
    }
}
