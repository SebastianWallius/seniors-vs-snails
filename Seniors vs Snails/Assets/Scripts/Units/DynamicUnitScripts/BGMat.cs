﻿using UnityEngine;
using System.Collections;

public class BGMat : MonoBehaviour {

    [SerializeField] private bool isSenior;
    public float speedModifier;


    void OnTriggerStay2D(Collider2D col) {
        if (System.Array.IndexOf(TileManager.materialTags, col.gameObject.tag) != -1) {
            if (!isSenior)
                speedModifier = TileManager.GetMaterialSpeedSnail(TileManager.matTagToTileType[col.gameObject.tag]);
            else
                speedModifier = TileManager.GetMaterialSpeedSenior(TileManager.matTagToTileType[col.gameObject.tag]);
        }
    }
}
