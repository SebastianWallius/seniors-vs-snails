﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowPointer : MonoBehaviour {

    public List<Transform> objectsToPointAt = new List<Transform>();
    public ArrowManager arrowMan;
    public GameObject graphics;
    public float currentAngle;

    public void AddObjectToPointAt(GameObject obj) {
        objectsToPointAt.Add(obj.transform);
        if (objectsToPointAt.Count == 1) {
            currentAngle = CalculateAngle(objectsToPointAt[0]);
        }
    }

    public void RemoveObjectToPointAt(GameObject obj) {
        objectsToPointAt.Remove(obj.transform);
    }

    
	void Update () {
        if (objectsToPointAt.Count > 0) {
            CheckIfRemoveSnail();
            PointAt();
        }
    }

    public void PointAt() {
        // If we have multiple objects to point at we choose the average direction of them all
        if (objectsToPointAt.Count > 1)
        {
            currentAngle = 0; 
            foreach (Transform pos in objectsToPointAt)
            {
                currentAngle += CalculateAngle(pos);
            }

            currentAngle = currentAngle / objectsToPointAt.Count;
        } else {
            currentAngle = CalculateAngle(objectsToPointAt[0]);
        }

        transform.localScale = Vector3.one;
        graphics.transform.localScale = new Vector3(Mathf.Clamp(0.5f + (objectsToPointAt.Count * 0.1f), 0.5f, 2f),
            Mathf.Clamp(0.5f + (objectsToPointAt.Count * 0.1f), 0.5f, 2f), 2f);

        // If the direction of the objectvector need to flip
        if (transform.position.x < objectsToPointAt[0].position.x) {
            currentAngle = currentAngle + 180;
        }

        if (currentAngle > 180) {
            currentAngle = currentAngle - 360f;
        }

        transform.rotation = Quaternion.Euler(0f, 0f, currentAngle);
    }

    bool CheckIfRemoveSnail() {
        if (objectsToPointAt.Count > 1)
        {
            for (int i = 0; i < objectsToPointAt.Count; i++)
            {
                if (currentAngle < 0 && (Mathf.Abs(CalculateAngle(objectsToPointAt[i]) - currentAngle) - 180f) > arrowMan.mergeAngle)
                {
                    arrowMan.snailToArrow.Remove(objectsToPointAt[i].gameObject);
                    objectsToPointAt.Remove(objectsToPointAt[i]);
                    CheckIfRemoveSnail();
                    return true;
                }   
                else if (currentAngle > 0 && Mathf.Abs(CalculateAngle(objectsToPointAt[i]) - currentAngle) > arrowMan.mergeAngle)
                {
                    arrowMan.snailToArrow.Remove(objectsToPointAt[i].gameObject);
                    objectsToPointAt.Remove(objectsToPointAt[i]);
                    CheckIfRemoveSnail();
                    return true;
                }
            }
        }

        if (objectsToPointAt.Count <= 0) {
            Destroy(this.gameObject);
        }
        return false;
    }

    float CalculateAngle(Transform refObject) {
        return (Mathf.Atan((transform.position.y - refObject.position.y) / (transform.position.x - refObject.position.x)) * (180f / Mathf.PI) + 90f);
    }
}
