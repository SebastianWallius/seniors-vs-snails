﻿using UnityEngine;
using System.Collections;

public class DynamicLayeringUnit : MonoBehaviour {

    [SerializeField] private SpriteRenderer[] unitSprites;
    [SerializeField] private Transform referenceTransform;
    [SerializeField] private bool isStatic = false;
    [SerializeField] private bool isPlayer = false;
    [SerializeField] private Transform handPoint;

    private Vector3 lastPos;

    void Start() {
        if (referenceTransform == null)
            referenceTransform = transform.parent;

        UpdateLayers();
    }
	
	// Update is called once per frame
	void Update () {
        if (!isStatic && referenceTransform.position != lastPos) {
            UpdateLayers();
        }
	}

    void UpdateLayers() {
        for (int i = 0; i < unitSprites.Length; i++)
        {
            unitSprites[i].sortingOrder = Mathf.RoundToInt(-referenceTransform.position.y * 50f) + i;
        }

        if (isPlayer && Profile.current.equippedWeapon != WeaponData.Weapon.Hands) {
            StartCoroutine(WaitSortPlayerHand());
        }

        lastPos = referenceTransform.position;
    }

    IEnumerator WaitSortPlayerHand() {
        yield return new WaitForEndOfFrame();
        handPoint.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(-referenceTransform.position.y * 50f);
    }
}
