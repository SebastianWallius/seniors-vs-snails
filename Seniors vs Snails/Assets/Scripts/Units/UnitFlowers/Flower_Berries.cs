﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Flower_Berries : MonoBehaviour , IFlower{

    // The type of flower
    private FlowersData.FlowerType typeOfFlower = FlowersData.FlowerType.Berries;
    public FlowersData.FlowerType TypeOfFlower
    {
        get { return typeOfFlower; }
    }

    // Flower general attributes
    [SerializeField] private float health;
    public float Health
    {
        get { return health; }
        set { health = value; }
    }

    [SerializeField] private float maxHealth;
    public float MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    [SerializeField] private int surviveReward; 
    public int SurviveReward
    {
        get { return surviveReward; }
        set { surviveReward = value; }
    }

    private bool isAlive;
    public bool IsAlive
    {
        get { return isAlive; }
        set { isAlive = value; }
    }

    public string StandardSpriteName {
        get { return FlowersData.GetStandardSpriteName(typeOfFlower); }
    }

    private GameObject flowerGameobject;
    public GameObject FlowerGameObject {
        get { return flowerGameobject; }
    }

    // Flower instance attributes and references
    private LevelManager levelMan = LevelManager.instance;
    [SerializeField] private Slider hitPointsSlider;
    [SerializeField] private ParticleSystem petalParticle;
    [SerializeField] private GameObject graphics;

    void Start()
    {
        // Initial set up
        typeOfFlower = FlowersData.FlowerType.Berries;
        flowerGameobject = this.gameObject;
        levelMan = LevelManager.instance;
        hitPointsSlider.maxValue = health;
        maxHealth = health;
        hitPointsSlider.gameObject.SetActive(false);
        isAlive = true;
    }

    void RemoveThisFlower()
    {
        isAlive = false;
        levelMan.RemoveFlowerFromList(this);
        this.gameObject.SetActive(false);
    }

    public void TakeDamage(float amt, GameObject attacker)
    {
        health -= amt;
        graphics.transform.localScale = new Vector3(1 - (((maxHealth - health) / maxHealth) / 2), 1 - (((maxHealth - health) / maxHealth) / 2), 1f);
        graphics.GetComponent<Animator>().SetTrigger("TakeDamage");
        petalParticle.Play();
        hitPointsSlider.gameObject.SetActive(true);
        hitPointsSlider.value = health;

        if (attacker.GetComponent<ISnail>() != null) {
            attacker.GetComponent<ISnail>().AddModifier(new SnailModifier(
                "Flower_Berries_Modifier", 
                SnailModifier.ModifierType.Positive, 
                8f, 0f, 0.2f, 0.4f, 0.2f));
        }

        if (health <= 0)
        {
            RemoveThisFlower();
        }
    }
}
