﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {

    // Will change!
    [SerializeField] private GameObject equippedWeapon;
    [SerializeField] private GameObject equippedSpecialAttack;
    [SerializeField] private Transform weaponHandPoint;
    [SerializeField] private Transform specialAttackPoint;
    [SerializeField] private BGMat bgMat;

    private GameObject currentTargetEnemy = null;

    private Animator anim;

    [SerializeField] private float speed = 1f;
    private bool facingLeft = true;
    public bool FacingLeft { get { return facingLeft; } }


	// Use this for initialization
	void Start () {
        // We need to get our currently equipped weapon
        anim = this.gameObject.GetComponent<Animator>();



        LoadEquippedWeapon();
        LoadEquippedSpecialAttack();
	}

    void LoadEquippedWeapon() {
        equippedWeapon = ItemsManager.instance.weapon_to_GO[Profile.current.equippedWeapon];

        GameObject weapon = (GameObject)GameObject.Instantiate(equippedWeapon, weaponHandPoint.position, Quaternion.identity);
        weapon.transform.parent = weaponHandPoint;
        speed = 2 + ((1f) - ((weapon.GetComponent<IWeapon>().Weight / 5)));
        anim.speed = 1 + ((equippedWeapon.GetComponent<IWeapon>().Swiftness/10) - (0.5f));
    }

    void LoadEquippedSpecialAttack() {
        if (Profile.current.equippedSpecialAttack != SpecialAttacksData.SpecialAttackType.None) {
            equippedSpecialAttack = ItemsManager.instance.specialAttackType_to_GO[Profile.current.equippedSpecialAttack];

            GameObject specialAttack = (GameObject)Instantiate(equippedSpecialAttack, specialAttackPoint.position, Quaternion.identity);
            specialAttack.transform.parent = specialAttackPoint;
            equippedSpecialAttack = specialAttack;
        }
    }

    // Update is called once per frame
    void Update () {
        // Movement:

        if (this.GetComponent<Rigidbody2D>().velocity.x > 0 && facingLeft || this.GetComponent<Rigidbody2D>().velocity.x < 0 && !facingLeft)
        {
            this.transform.localScale = new Vector2(-this.transform.localScale.x, this.transform.localScale.y);
            facingLeft = !facingLeft;
        }

        // Animation
        GetComponent<Animator>().SetFloat("CurrentSpeed", GetComponent<Rigidbody2D>().velocity.magnitude);

        //this.GetComponent<Animator>().speed = Mathf.Clamp(this.GetComponent<Rigidbody2D>().velocity.magnitude, 0f, 1f);

        if (CrossPlatformInputManager.GetButton("m_Attack") && anim.GetBool("IsAttacking") == false) {
            Attack();
        }

        if (currentTargetEnemy != null && currentTargetEnemy.GetComponent<ISnail>().Health <= 0) {
            currentTargetEnemy = null;
        }

	}

    void FixedUpdate() {
        
        GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Lerp(0, CrossPlatformInputManager.GetAxis("m_Horizontal") * speed * bgMat.speedModifier, 0.8f), 
            Mathf.Lerp(0, CrossPlatformInputManager.GetAxis("m_Vertical") * speed * bgMat.speedModifier, 1f));

        // If moving the character, we need to move
        if (this.GetComponent<Rigidbody2D>().velocity.magnitude > speed)
            this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.normalized * speed * bgMat.speedModifier;

        // If not moving character, we need to stop
        if (new Vector2(CrossPlatformInputManager.GetAxis("m_Horizontal"), CrossPlatformInputManager.GetAxis("m_Vertical")).magnitude == 0)
            this.GetComponent<Rigidbody2D>().velocity = Vector2.Lerp(this.GetComponent<Rigidbody2D>().velocity, Vector2.zero, 0.8f);
    }

    void Attack() {
        anim.SetBool("IsAttacking", true);
        SoundManager.instance.PlaySoundFx(SoundManager.instance.senior_Attack_Miss);
    }

    // Called from the animation
    void DamageTarget() {
        anim.SetBool("IsAttacking", false);

        if (currentTargetEnemy != null && currentTargetEnemy.GetComponent<ISnail>().Health >= 1) {
            currentTargetEnemy.GetComponent<ISnail>().TakeDamage(equippedWeapon.GetComponent<IWeapon>().Damage, this.gameObject);
            Vibration.Vibrate((long)equippedWeapon.GetComponent<IWeapon>().Damage);
            SoundManager.instance.PlaySoundFx(SoundManager.instance.senior_Attack_Hit);


            if (currentTargetEnemy.GetComponent<ISnail>().Health <= 0)
                currentTargetEnemy = null; // Our target is dead and we need to find a new one
        }
    }

    public void SeniorWalkSound() {
        SoundManager.instance.PlaySoundFx(SoundManager.instance.senior_Walk);
    }

    public GameObject GetEquippedWeapon()
    {
        return equippedWeapon;
    }

    public GameObject GetEquippedSpecialAttack() {
        return equippedSpecialAttack;
    }

    void OnTriggerStay2D(Collider2D col) {
        if (col.gameObject.tag == "Enemy" && currentTargetEnemy == null) {
            currentTargetEnemy = col.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject == currentTargetEnemy)
            currentTargetEnemy = null;
    }

}
