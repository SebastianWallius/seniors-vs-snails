﻿using System.Collections.Generic;

public static class FlowersData {

    public static Dictionary<FlowerType, SeedData.SeedType> flowerTypeToSeedType = new Dictionary<FlowerType, SeedData.SeedType>() {
        { FlowerType.Basic, SeedData.SeedType.Basic },
        { FlowerType.Cactus, SeedData.SeedType.Cactus},
        { FlowerType.Berries, SeedData.SeedType.Berries}
    };

    public enum FlowerType
    {
        Basic,
        Cactus,
        Berries
    }

    public static string GetStandardSpriteName(FlowerType type) {
        switch (type) {
            case FlowerType.Basic :
                return "Flower_Basic";
            case FlowerType.Cactus :
                return "Flower_Cactus";
            case FlowerType.Berries :
                return "Flower_Berries";
        }
        return "";
    }
}
