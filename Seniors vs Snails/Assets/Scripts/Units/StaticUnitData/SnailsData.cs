﻿public static class SnailsData {

    // All snail types
    public enum SnailType
    {
        Basic,
        Grunt,
        Catapult
    }

    public static string GetStandardSpriteName(SnailType type)
    {
        switch (type)
        {
            case SnailType.Basic:
                return "Snail_Basic";
            case SnailType.Grunt:
                return "Snail_Grunt";
            case SnailType.Catapult:
                return "Snail_Catapult";
        }
        return "";
    }
}
