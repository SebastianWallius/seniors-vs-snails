﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


public class Snail_MrBossHat: MonoBehaviour, ISnail {

    // Interface attributes
    private SnailsData.SnailType typeOfSnail = SnailsData.SnailType.Basic;
    public SnailsData.SnailType TypeOfSnail
    {
        get { return typeOfSnail; }
    }

    [SerializeField] private float health;
    public float Health
    {
        get { return health; }
        set { health = value; }
    }

    [SerializeField] private float damage;
    public float Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    [SerializeField] private float speed;
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    [SerializeField] private int xpGain;
    public int XpGain
    {
        get { return xpGain; }
        set { xpGain = value; }
    }

    [SerializeField] private float attackDistance;
    public float AttackDistance
    {
        get { return attackDistance; }
        set { attackDistance = value; }
    }

    [SerializeField] private float animationSpeed;
    public float AnimationSpeed
    {
        get { return animationSpeed; }
        set { animationSpeed = value; }
    }

    [SerializeField]
    private List<SnailModifier> snailModifiers = new List<SnailModifier>();
    public List<SnailModifier> SnailModifiers
    {
        get { return snailModifiers; }
        set { snailModifiers = value; }
    }

    [SerializeField]
    private List<SnailModifier> blockedModifiers = new List<SnailModifier>();
    public List<SnailModifier> BlockedSnailModifiers
    {
        get { return blockedModifiers; }
        set { blockedModifiers = value; }
    }

    // Interface booleans
    private bool isSpawning;
    public bool IsSpawning
    {
        get { return isSpawning; }
    }

    private bool isAlive;
    public bool IsAlive
    {
        get { return isAlive; }
    }

    private bool isAttacking;
    public bool IsAttacking {
        get { return isAttacking; }
    }


    // Interface external references
    public string StandardSpriteName
    {
        get { return SnailsData.GetStandardSpriteName(typeOfSnail); }
    }

    [SerializeField] private SpriteRenderer spriteRenderer;
    public SpriteRenderer SpriteRenderer
    {
        get { return spriteRenderer; }
    }

    private IFlower targetFlower;
    public IFlower TargetFlower
    {
        get { return targetFlower; }
        set { targetFlower = value; }
    }

    private GameObject snailGameObject;
    public GameObject SnailGameObject
    {
        get { return snailGameObject; }
    }


    // Instance internal references 
    [SerializeField] private Slider hitPointsSlider;
    [SerializeField] private GameObject slimeParticle;
    private PolyNavAgent navAgent;
    private Animator anim;
    private BGMat bgMat;

    // Instance external references
    private Vector3 spawnPoint;
    private LevelManager levelMan;

    // Instance state booleans
    [HideInInspector] public bool playerIsNear;
    bool facingLeft;

    // Instance variables
    float stunTime;
    float orginalHealth;
    float orginalDamage;
    float orginalSpeed;
    float orginalAnimationSpeed;


    // Called when snail is spawned
    void Start () {
        // Initial set up references
        typeOfSnail = SnailsData.SnailType.Basic;
        snailGameObject = this.gameObject;
        anim = this.GetComponent<Animator>();
        navAgent = GetComponent<PolyNavAgent>();
        bgMat = GetComponentInChildren<BGMat>();
        levelMan = LevelManager.instance;

        // Initial set up values
        spawnPoint = this.transform.position;
        hitPointsSlider.maxValue = health;
        hitPointsSlider.gameObject.SetActive(false);
        anim.speed = animationSpeed;
        isAlive = true;
        stunTime = 0f;
        orginalHealth = health;
        orginalDamage = damage;
        orginalSpeed = speed;
        orginalAnimationSpeed = animationSpeed;

    }
	
	// Update is called once per frame
	void Update () {
        // States:
        // 1: find target flower (check if flower is still alive)
        // 2: go to target flower (check if flower is still alive)
        // 3: if close enough, attack (check if flower is still alive)
        // 4: we don't have a target and need to roam to spawn

        if (isAlive && !isSpawning)
        {
            // State 1: We want to find a target
            if (levelMan.FlowersStillInScene() > 0) {
                if (targetFlower == null || targetFlower.Health <= 0) {
                    FindTarget();
                }

                // State 2: We have a target but is too far away to attack
                if (stunTime > 0) {
                    stunTime -= Time.deltaTime;
                    navAgent.Stop(); 
                }
                else if (targetFlower != null && TargetDistance() > attackDistance && !isAttacking)
                {
                    GoToTarget();
                }
                else if (targetFlower != null && targetFlower.Health > 0 && TargetDistance() < attackDistance)
                {
                    // State 3: We stop and attack
                    StopMovement();
                    Attack();
                }

            } else {
                // State 4: No flowers left in the scene, we go to spawn
                GoBackToSpawn();
            }
            


        } else if (isAlive && isSpawning && targetFlower == null) {
            // If we are currently spawning, we need to find a target so we are facing a flower when spawning
            FindTarget();
            FaceTarget();

        } else { StopMovement(); }



        ///////////////// MODIFIERS ///////////////////
        // Here we count down the snailModifiers

        if (snailModifiers.Count > 0)
        {
            foreach (SnailModifier mod in snailModifiers)
            {
                mod.modifierTime -= Time.deltaTime;
            }
        }

        if (blockedModifiers.Count > 0)
        {
            foreach (SnailModifier mod in blockedModifiers)
            {
                mod.modifierTime -= Time.deltaTime;
            }
        }

        // Check if we need to remove anything

        for (int i = 0; i < snailModifiers.Count; i++)
        {
            if (snailModifiers[i].modifierTime <= 0f)
            {
                SnailModifiers.Remove(snailModifiers[i]);
                i--;
                UpdateModifiers();
            }
        }

        for (int i = 0; i < blockedModifiers.Count; i++)
        {
            if (blockedModifiers[i].modifierTime <= 0f)
            {
                blockedModifiers.Remove(blockedModifiers[i]);
                i--;
                UpdateModifiers();
            }
        }
    }

    void FixedUpdate() {
        Collider2D[] test = Physics2D.OverlapCircleAll(transform.position, 2f);
        foreach (Collider2D obj in test) {
            if (obj.gameObject.tag == "Enemy") {
                obj.GetComponent<ISnail>().AddModifier(new SnailModifier("Area8BossTest", SnailModifier.ModifierType.Positive, 5f, 0f, 0.4f, 0.5f, 0.2f));
            }
        }
    }

    // Returns the distance to the target
    float TargetDistance() {
        if (targetFlower != null)
        {
            return Vector3.Distance(targetFlower.FlowerGameObject.transform.position, transform.position);
        } else {
            return Mathf.Infinity;
        }
    }

    // Called from animation!
    public void FinishedSpawning()
    {
        isSpawning = false;
    }


    // Finds a target flower
    private void FindTarget() {
        targetFlower = levelMan.GetClosestFlower(transform.position);
    }

    // Faces the target flower, called before spawning
    private void FaceTarget() {
        if ((targetFlower.FlowerGameObject.transform.position - this.transform.position).x > 0)
        {
            this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, 1f);
            slimeParticle.transform.localScale = new Vector3(-slimeParticle.transform.localScale.x, slimeParticle.transform.localScale.y, 1f);
            hitPointsSlider.transform.parent.localScale = new Vector3(-hitPointsSlider.transform.localScale.x, hitPointsSlider.transform.localScale.y, 1f);

            facingLeft = !facingLeft;
        }
    }

    // Checks the current direcion of the navAgent and face the moving direction
    private void CheckDirection() {
        if ((navAgent.movingDirection.x < 0 && facingLeft || navAgent.movingDirection.x > 0 && !facingLeft))
        {
            this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, 1f);
            slimeParticle.transform.localScale = new Vector3(-slimeParticle.transform.localScale.x, slimeParticle.transform.localScale.y, 1f);
            hitPointsSlider.transform.parent.localScale = new Vector3(-hitPointsSlider.transform.parent.localScale.x, hitPointsSlider.transform.parent.localScale.y, 1f);

            facingLeft = !facingLeft;
        }
    }

    // Goes back to spawn, called when all flowers are destroyed
    private void GoBackToSpawn() {
        navAgent.SetDestination(spawnPoint);
        navAgent.maxSpeed = (speed * bgMat.speedModifier)/2;

        if (!isAttacking)
            CheckDirection();
    }

    // Goes to the target flower
    private void GoToTarget() {
        navAgent.SetDestination(targetFlower.FlowerGameObject.transform.position);
        navAgent.maxSpeed = speed * bgMat.speedModifier;

        if(!isAttacking)
            CheckDirection();
    }

    // Stops movement
    private void StopMovement() {
        navAgent.Stop();
    }

    // Attack the target, called when close to the target flower
    private void Attack() {
        // StartAnimation
        isAttacking = true;
        anim.SetBool("IsAttacking", true);

        // The call to damage the flower is called from the animation
    }

    // Updates the state to finish an attack, called from animation
    public void AttackFinished() {
        isAttacking = false;
    }

    // Called from the animation when the snail is biting
    public void DamageTargetFlower() {
        anim.SetBool("IsAttacking", false);

        if (targetFlower != null) {
            if (targetFlower.Health > 0 && Vector3.Distance(targetFlower.FlowerGameObject.transform.position, transform.position) < 0.5f)
            {
                targetFlower.TakeDamage(damage, this.gameObject);
            }
        }
    }


    // Trigger detection
    void OnTriggerStay2D(Collider2D col) {
        if(col.gameObject.tag == "Player") {
            playerIsNear = true;
        }
    }

    // Trigger detection
    void OnTriggerExit2D(Collider2D col)     {
        if (col.gameObject.tag == "Player")
        {
            playerIsNear = false;
        }
    }






    // Interface methods:
    // Called from the player or the enviroment
    public void TakeDamage(float amt, GameObject attacker)
    {
        health -= amt;
        hitPointsSlider.gameObject.SetActive(true);
        hitPointsSlider.value = health;

        if (health <= 0)
        {
            KillImmediate();
            hitPointsSlider.value = 0f;
        }
        else
        {
            //PhysicsHitImpact(Vector3.zero, amt);
        }
    }

    public void Stun(float time)
    {
        stunTime += time;
    }

    public void AddModifier(SnailModifier newModifier)
    {
        // This function adds the enraged modifier to a snail. An enraged snail deals more damage and is quicker.
        // To display this effect, the snail graphics will expand and the color of the snail will become red.
        // A snail can have different types of enrage
        // Situations we will need to handle:
        if (newModifier.modifierTime > 0)
        {

            // First we check if the modifier already is active on the snail:
            bool modIsActive = false;
            foreach (SnailModifier mod in snailModifiers)
            {
                if (mod.modifierName == newModifier.modifierName)
                {
                    modIsActive = true;
                    // The modifier is active on the snail, just reset the time if the new time exeeds the previous
                    if (newModifier.modifierTime > mod.modifierTime)
                    {
                        mod.modifierTime = newModifier.modifierTime;
                    }
                }
            }

            if (!modIsActive)
            {

                // If the modifier is not active, then we need to add it
                // But first we must look if the modifierType is not blocked
                bool modIsBlocked = false;
                foreach (SnailModifier mod in blockedModifiers)
                {
                    if (newModifier.modifierType == mod.modifierType)
                    {
                        modIsBlocked = true;
                    }
                }

                if (!modIsBlocked)
                {
                    SnailModifiers.Add(newModifier);
                }
            }
        }

        UpdateModifiers();
    }

    public void RemoveModifierType(SnailModifier.ModifierType type, float blockModifierTypeForTime)
    {
        // This function removes all modifiers of a given type (positive, negative or balanced)
        // It can also block the modifier type for activating again for a certain period

        for (int i = 0; i < snailModifiers.Count; i++)
        {
            if (snailModifiers[i].modifierType == type)
            {
                SnailModifiers.Remove(snailModifiers[i]);
                i--;
            }
        }

        if (blockModifierTypeForTime > 0f)
        {
            // We need to block this type of modifier to activate again for the given time
            blockedModifiers.Add(new SnailModifier("BlockedType", type, blockModifierTypeForTime, 0f, 0f, 0f, 0f));
        }

        UpdateModifiers();
    }

    public void UpdateModifiers()
    {
        // This function checks all the active modifiers and add it to the snail
        // It should calculate this whenever we add or remove an active modifier
        float modHealth = 0;
        float modDamage = 0;
        float modSpeed = 0;
        float modAnimationSpeed = 0;

        foreach (SnailModifier mod in SnailModifiers)
        {
            // Health
            modHealth += (orginalHealth * mod.healthMultiplier);

            // Damage
            modDamage += (orginalDamage * mod.damageMultiplier);

            // Speed
            modSpeed += (orginalSpeed * mod.speedMultiplier);

            // AnimationSpeed
            modAnimationSpeed += (orginalAnimationSpeed * mod.animationSpeedMultiplier);

        }

        // Add to snail:
        //health += modHealth;
        damage = orginalDamage + modDamage;
        speed = orginalSpeed + modSpeed;
        animationSpeed = orginalSpeed + modAnimationSpeed;

    }

    public void PhysicsHitImpact(Vector3 hitPos, float amt)
    {
        GetComponent<Rigidbody2D>().AddForce((this.transform.position - hitPos).normalized * amt, ForceMode2D.Impulse);
    }

    // Called when health has reached zero
    public void KillImmediate()
    {
        isAlive = false;
        anim.SetBool("IsDead", true);
        levelMan.RemoveSnailFromList(this.gameObject);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        hitPointsSlider.gameObject.SetActive(false);

        // Spawn money
        levelMan.GetComponent<RewardManager>().SpawnMoneyFromTarget(25, 1, 1, transform.position);
        levelMan.GetComponent<RewardManager>().SpawnSeedFromTarget(15, SeedData.SeedType.Basic, transform.position);

    }
}
