﻿using UnityEngine;
using System.Collections.Generic;

public interface ISnail {

    // The snail type
    SnailsData.SnailType TypeOfSnail
    {
        get;
    }

    // Snail attributes
    float Health {
        get;
        set;
    }

    float Damage {
        get;
        set;
    }

    float Speed {
        get;
        set;
    }

    int XpGain {
        get;
        set;
    }

    float AnimationSpeed {
        get;
        set;
    }

    float AttackDistance {
        get;
        set;
    }

    List<SnailModifier> SnailModifiers {
        get;
        set;
    }

    List<SnailModifier> BlockedSnailModifiers {
        get;
        set;
    }

    // State booleans
    bool IsSpawning {
        get;
    }

    bool IsAlive {
        get;
    }

    bool IsAttacking {
        get;
    }
    
    // External references
    string StandardSpriteName {
        get;
    }

    IFlower TargetFlower {
        get;
        set;
    }

    // For checking if the snail is currently rendering (if inside camera view)
    SpriteRenderer SpriteRenderer
    {
        get;
    }

    GameObject SnailGameObject
    {
        get;
    }


    void TakeDamage(float amt, GameObject attacker);
    void Stun(float time);
    void AddModifier(SnailModifier modifier);
    void RemoveModifierType(SnailModifier.ModifierType type, float blockModifierTypeForTime);
    void UpdateModifiers();
    void PhysicsHitImpact(Vector3 hitPos, float amt);
    void KillImmediate();
}
