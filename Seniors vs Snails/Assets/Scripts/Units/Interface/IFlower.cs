﻿using UnityEngine;

public interface IFlower {

    // Flower interface data
    FlowersData.FlowerType TypeOfFlower {
        get;
    }

    float Health {
        get;
        set;
    }

    float MaxHealth {
        get;
        set;
    }

    bool IsAlive {
        get;
        set;
    }

    int SurviveReward {
        get;
        set;
    }

    string StandardSpriteName {
        get;
    }

    GameObject FlowerGameObject {
        get;
    }

    // Here we define everything that can happen to a flower
    void TakeDamage(float amt, GameObject attacker);
}
