﻿using UnityEngine;
public class SnailModifier {

    // Name (For keeping track of modifiers, we do not stack modifiers of the same type, but instead we only reset the time)
    public string modifierName;

    // ModifierType
    public ModifierType modifierType; 
    public enum ModifierType {
        Positive,
        Negative,
        Balanced
    }

    // Time
    public float modifierTime;

    // Health
    public float healthMultiplier;

    // Damage
    public float damageMultiplier;

    // Speed
    public float speedMultiplier;

    // Animationspeed
    public float animationSpeedMultiplier;

    public SnailModifier(string name, ModifierType type, float time, float healthMult, float damageMult, float speedMult, float animationSpeedMult) {
        modifierName = name;
        modifierType = type;
        modifierTime = time;
        healthMultiplier = healthMult;
        damageMultiplier = damageMult;
        speedMultiplier = speedMult;
        animationSpeedMultiplier = animationSpeedMult;
    }
}
