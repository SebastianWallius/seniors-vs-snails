﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialLevel : MonoBehaviour {

    public static TutorialLevel instance;

    public TutorialManager.TutorialInfo tutorialInfo;
    [SerializeField] private GameObject oldMan_GO;
    [SerializeField] private GameObject talkBubble_GO;
    [SerializeField] private Text talkBubbleText;
    [SerializeField] private Button talkBubble_Button;



    private string[] savedTutorialTexts;
    private int currentDisplayedText;

    void Awake() {
        instance = this;
    }


    void Start() {
        if (tutorialInfo != TutorialManager.TutorialInfo.None)
        {
            // We want a tutorial to play
            savedTutorialTexts = TutorialManager.tutorialInfoToTexts[tutorialInfo];

            // Animate OldMan
            oldMan_GO.SetActive(true);
            oldMan_GO.GetComponent<Animator>().SetTrigger("PopIn");

            // Animate talkBubble
            talkBubble_GO.SetActive(true);
            talkBubble_Button.interactable = false;

            currentDisplayedText = 0;
            StartCoroutine(WaitForSenior(oldMan_GO.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length));
        }
        else {
            oldMan_GO.SetActive(false);
            talkBubble_GO.SetActive(false);
        }
    }

    IEnumerator WaitForSenior(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        StartCoroutine(TalkBubblePop(false, true));
    }

    IEnumerator TalkBubblePop(bool popOut, bool popIn)
    {
        if (popOut) {
            talkBubble_Button.interactable = false;
            talkBubble_GO.GetComponent<Animator>().SetTrigger("PopOut");
            yield return new WaitForSeconds(talkBubble_GO.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        }

        if (popIn)
        {
            talkBubbleText.text = savedTutorialTexts[currentDisplayedText];
            talkBubble_GO.GetComponent<Animator>().SetTrigger("PopIn");
            talkBubble_Button.interactable = true;
            yield return new WaitForSeconds(talkBubble_GO.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        }
    }

    public void ContinueTutorial() {
        if ((currentDisplayedText + 1) < savedTutorialTexts.Length) {
            // We still have more text to show
            currentDisplayedText++;
            StartCoroutine(TalkBubblePop(true, true));
        } else {
            StartCoroutine(TalkBubblePop(true, false));
            oldMan_GO.GetComponent<Animator>().SetTrigger("PopOut");
            StartCoroutine(WaitForEnableSpawning());
        }
    }

    IEnumerator WaitForEnableSpawning() {
        yield return new WaitForSeconds(0.5f);
        LevelManager.instance.EnableSnailSpawning();
    }
}
