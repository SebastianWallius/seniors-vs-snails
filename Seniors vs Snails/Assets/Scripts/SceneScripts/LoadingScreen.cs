﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour {



    void Start()
    {
        GetComponent<ProfileSelect>().ShowProfileUI(false);
        GetComponent<ProfileSelect>().ShowNewProfileUI(false);
        StartCoroutine(WaitProfileSelect(2f));
    }

    IEnumerator WaitProfileSelect(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        GetComponent<ProfileSelect>().ShowProfileUI(true);
    }


}
