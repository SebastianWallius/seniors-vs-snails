﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldView : MonoBehaviour {

    [SerializeField] private Button[] levelButtons;

    void Start() {
        EnableLevelsForPlayer();
    }


    void EnableLevelsForPlayer() {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            if (Profile.current.currentWorldLevel > i)
            {
                levelButtons[i].interactable = true;
            }
            else {
                levelButtons[i].interactable = false;
            }
        }
    }
}
