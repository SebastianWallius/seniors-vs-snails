﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileSelect : MonoBehaviour {

    [SerializeField] private GameObject uIProfileSelect;
    [SerializeField] private GameObject uINewProfile;
    [SerializeField] private InputField newProfileName;
    [SerializeField] private Text[] profile_1Stats;
    [SerializeField] private Text[] profile_2Stats;
    [SerializeField] private Text[] profile_3Stats;
    private Text[][] allProfiles = new Text[3][];
    
    // Use this for initialization
    void Start () {
        allProfiles[0] = profile_1Stats;
        allProfiles[1] = profile_2Stats;
        allProfiles[2] = profile_3Stats;

        ShowNewProfileUI(false);
        UpdateProfileUI();
    }

    public void UpdateProfileUI() {
        for (int i = 0; i < 3; i++) {
            Profile profileData = ProfileManager.instance.GetProfileInfo(i);

            if (profileData != null) {
                // A game save file exist and we need to load from it

                // Header
                allProfiles[i][0].text = profileData.profileName;
                // Empty text
                allProfiles[i][1].gameObject.SetActive(false);
                allProfiles[i][1].text = "";
                // Level stat
                allProfiles[i][2].gameObject.SetActive(true);
                allProfiles[i][2].text = "Level: " + profileData.currentWorldLevel.ToString();
                // Money stat
                allProfiles[i][3].gameObject.SetActive(true);
                allProfiles[i][3].text = "Money: " + profileData.money.ToString() + "$";

            } else {// No save file exist

                // Header
                allProfiles[i][0].text = "Profile " + (i + 1).ToString();
                // Empty text
                allProfiles[i][1].gameObject.SetActive(true);
                allProfiles[i][1].text = "Empty";
                // Level stat
                allProfiles[i][2].gameObject.SetActive(false);
                // Money stat
                allProfiles[i][3].gameObject.SetActive(false);
            }
        }
    }

    public void SelectProfile(int profile) {
        if (ProfileManager.instance.SelectProfile(profile)) {

            ChangeScene();
        } else {
            ShowNewProfileUI(true);
            ShowProfileUI(false);
        }
    }

    public void CreateNewProfile() {
        if (newProfileName.text != "") {
            ProfileManager.instance.CreateNewProfile(newProfileName.text);
            ChangeScene();
        }
    }

    public void ShowNewProfileUI(bool show) {
        uINewProfile.gameObject.SetActive(show);
    }

    public void ShowProfileUI(bool show) {
        uIProfileSelect.SetActive(show);

    }

    public void ChangeScene()
    {
        GameManager.instance.ChangeScene("MainMenu");
    }
}
