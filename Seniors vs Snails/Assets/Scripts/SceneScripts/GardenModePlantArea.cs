﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GardenModePlantArea : MonoBehaviour {

    [SerializeField] private Transform seedParent;
    [SerializeField] private GameObject plantAreaDirt;
    [SerializeField] private Button plantButton;
    [SerializeField] private Button harvestButton;
    [SerializeField] private Text countDownText;

    // Instance references
    private ISeed seedRef;
    [HideInInspector] public int position;
    private bool canHarvest;

    public void DisplayPlant(ISeed seed) {
        // Here we get a seed to display. It could either be a newly planted seed OR we might have to display
        // a seed/plant from a previous session.
        plantButton.gameObject.SetActive(false);

        // Let's first create the gameObject that displays the seed
        GameObject seed_GO = (GameObject)Instantiate(ItemsManager.instance.seedType_to_GO[seed.TypeOfSeed],
            transform.position, Quaternion.identity);
        seed_GO.transform.parent = seedParent;
        seed_GO.GetComponent<ISeed>().ParentPlantArea = this;
        seedRef = seed_GO.GetComponent<ISeed>();

        // We need to check if the ISeed has been planted before:
        if (seed.PlantTime == DateTime.MinValue) {
            // Then update the time left to be the one stored in this ISeed
            seedRef.CurrentGrowTimeLeft = seed.TimeToGrow;
        }
        else {
            // However if the plant has been planted in an another session, then we need to calculate the time diff:
            seedRef.CurrentGrowTimeLeft = seedRef.TimeToGrow - CalculateTimeDifference(seed);
        }

        plantAreaDirt.SetActive(true);
        countDownText.gameObject.SetActive(true);

        countDownText.text = string.Format("{0:00}:{1:00}:{2:00}", seedRef.CurrentGrowTimeLeft.TotalHours, seedRef.CurrentGrowTimeLeft.Minutes, seedRef.CurrentGrowTimeLeft.Seconds);

    }

    void Update() {
        if (seedRef != null) {
            // Here we display the time left until the plant is finished growing
            // We need to parse seconds for this

            if (seedRef.CurrentGrowTimeLeft.TotalHours > 1)
            {
                countDownText.text = string.Format("{0:00}:{1:00}:{2:00}", seedRef.CurrentGrowTimeLeft.TotalHours, seedRef.CurrentGrowTimeLeft.Minutes, seedRef.CurrentGrowTimeLeft.Seconds);
            }
            else {
                countDownText.text = string.Format("{0:00}:{1:00}", seedRef.CurrentGrowTimeLeft.Minutes, seedRef.CurrentGrowTimeLeft.Seconds);
            }


        }
    }


    TimeSpan CalculateTimeDifference(ISeed seed) {
        // Here we calculate the time difference between the plant time and our current time
        if (seed.PlantTime == null) {
            return TimeSpan.Zero;
        } else {
            TimeSpan diff = DateTime.Now - seed.PlantTime;

            return diff;
        }
    }

    // Called from the seed when it has finished growing
    public void PlantIsFinishedGrowing(ISeed seed) {
        if (seed == seedRef) { // To make sure that the call was from our reference seed
            countDownText.gameObject.SetActive(false);
            canHarvest = true;
        }
    }

    public void ButtonPlant() {
        plantButton.gameObject.SetActive(false);
        GardenModeManager.instance.DisplaySeedPicker(position);
    }

    public void ButtonHarvest()
    {
        if(canHarvest){
            canHarvest = false;
            RewardManager.instance.SpawnMoneyFromTarget(100, 4, 4, seedParent.position);

            if (Profile.current.plantAtPos.ContainsKey(position))
            {
                Profile.current.plantAtPos.Remove(position);
            }
            else {
                Debug.LogError("No plant at position " + position + " could be found!");
            }


            Destroy(seedParent.GetChild(0).gameObject);
            plantAreaDirt.SetActive(false);
            harvestButton.gameObject.SetActive(false);
            seedRef = null;
        }
    }

    public void OnTriggerStay2D(Collider2D col) {
        if (seedRef == null && col.gameObject.tag == "Player") {
            plantButton.gameObject.SetActive(true);
        } else if (seedRef != null && canHarvest && col.gameObject.tag == "Player") {
            harvestButton.gameObject.SetActive(true);
        }
    }

    public void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.tag == "Player") {
            plantButton.gameObject.SetActive(false);
            harvestButton.gameObject.SetActive(false);
        }
    }
}
