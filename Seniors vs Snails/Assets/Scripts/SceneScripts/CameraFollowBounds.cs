﻿using UnityEngine;
using System.Collections;

public class CameraFollowBounds : MonoBehaviour {

    [SerializeField] private bool followPlayer;
    [SerializeField] private Transform player;

    private MapTileGenerator map;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

	// Use this for initialization
	void Start () {
        if (player == null)
            player = GameObject.Find("Senior").transform;

        float vertExtent = Camera.main.orthographicSize;
        float horzExtent = vertExtent * ((Screen.width*1.0f) / (Screen.height*1.0f));
        map = MapTileGenerator.instance;
        bool xIsEven = (map.currentTiles_x % 2 == 0);
        bool yIsEven = (map.currentTiles_y % 2 == 0);


        minX = horzExtent - map.currentTiles_x;
        maxX = map.currentTiles_x - horzExtent;
        minY = vertExtent - map.currentTiles_y;
        maxY = map.currentTiles_y - vertExtent;

        if (xIsEven)
        {
            minX++;
            maxX++;
        }

        if (yIsEven) {
            minY--;
            maxY--;
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (followPlayer) {
            Vector3 camPos = new Vector3(player.position.x, player.position.y, -1f);
            camPos.x = Mathf.Clamp(camPos.x, minX, maxX);
            camPos.y = Mathf.Clamp(camPos.y, minY, maxY);

            transform.position = Vector3.Lerp(transform.position, camPos, 0.02f);
        }
	}
}
