﻿using UnityEngine;
using System.Collections;

public class CompentusLogo : MonoBehaviour {

    [SerializeField] private string scene;
    [SerializeField] private Animator anim;
    private float waitTime;


    // Use this for initialization
	void Start () {

        AnimatorClipInfo[] myAnimatorClip = anim.GetCurrentAnimatorClipInfo(0);

        waitTime = myAnimatorClip[0].clip.length;

        StartCoroutine(ChangeScene());
    }

    // Update is called once per frame
    void Update () {
	
	}

    IEnumerator ChangeScene() {
        yield return new WaitForSeconds(waitTime);
        GameManager.instance.ChangeScene(scene);
    }
}
