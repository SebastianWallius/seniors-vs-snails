﻿using UnityEngine;
using UnityEngine.UI;

public class SeedWindow : MonoBehaviour {

    public Text window_SeedName;
    public Image window_SeedImage;
    public Image window_FlowerImage;
    public Text window_GrowTime;
    public Text window_Reward;
    public Text window_SeedInvCount;

    private ISeed refSeed;


    public void DisplaySeed(ISeed seed) {
        refSeed = seed;
        window_SeedName.text = seed.Name;
        window_SeedImage.sprite = Resources.Load(seed.SeedImageName, typeof(Sprite)) as Sprite;
        window_FlowerImage.sprite = Resources.Load(seed.FlowerImageName, typeof(Sprite)) as Sprite;

        if (seed.TimeToGrow.Minutes > 60)
        {
            window_GrowTime.text = Mathf.Floor(seed.TimeToGrow.Minutes/ 60) + " h : " + 
                Mathf.RoundToInt(seed.TimeToGrow.Minutes % 60) + " minutes";
        }
        else {
            window_GrowTime.text = seed.TimeToGrow.Minutes.ToString() + " minutes";
        }

        window_Reward.text = "+ " + seed.MoneyReward.ToString();
        window_SeedInvCount.text = "x " + Profile.current.ownedSeeds[seed.TypeOfSeed].ToString();
    }

    public void ChooseSeed() {
        GardenModeManager.instance.PlantSeedInPlantArea(refSeed.TypeOfSeed);
    }
}
