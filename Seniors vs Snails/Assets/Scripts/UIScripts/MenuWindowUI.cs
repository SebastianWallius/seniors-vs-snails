﻿using UnityEngine;
using System.Collections;

public class MenuWindowUI : MonoBehaviour {

    [SerializeField] private GameObject menuWindow;

    void Start() {
        menuWindow.SetActive(false);
    }

    public void ToggleWindow() {
        if (menuWindow.activeInHierarchy)
        {
            menuWindow.SetActive(false);
            GameManager.instance.UnPauseGame();
        }
        else {
            menuWindow.SetActive(true);
            GameManager.instance.PauseGame();
        }
    }

    public void ContinueButton() {
        ToggleWindow();
    }

    public void MainMenuButton() {
        GameManager.instance.ChangeScene("MainMenu");
    }

    public void RestartButton() {
        GameManager.instance.RestartScene();
    }

    public void ChangeMusicVolume() {

    }

    public void ChangeSoundFX() {

    }


}
