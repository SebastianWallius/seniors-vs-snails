﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyUpdate : MonoBehaviour {

    [SerializeField] private bool updateStoredMoneyAsWell = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (updateStoredMoneyAsWell)
        {
            GetComponent<Text>().text = (Profile.current.money + RewardManager.instance.GetStoredMoney()).ToString();
        }
        else {
            GetComponent<Text>().text = Profile.current.money.ToString();
        }
        
	}
}
