﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpecialAttackWindow : MonoBehaviour {

    private ISpecialAttack refspecialAttack;
    [HideInInspector] public ShopManager shopMan;

    public Text window_SpecialAttackName;
    public Image window_SpecialAttackImage;
    public Text window_SpecialAttackType;
    public Text window_Description;
    public Button costButton;
    public GameObject window_LockGO;
    public Text window_LevelLock;


    // Displays given specialAttack
    public void DisplaySpecialAttack(ISpecialAttack specialAttack) {
        refspecialAttack = specialAttack;
        window_SpecialAttackName.text = specialAttack.Name;
        window_SpecialAttackImage.sprite = Resources.Load(specialAttack.ImageName, typeof(Sprite)) as Sprite;
        window_SpecialAttackType.text = specialAttack.TypeOfSpecialAttack.ToString();
        window_Description.text = specialAttack.Description;


        // Check Ownership / Cost
        if (Profile.current.equippedSpecialAttack.Equals(specialAttack.ID)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        } else if (Profile.current.ownedSpecialAttacks.Contains(specialAttack.TypeOfSpecialAttack)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        } else {
            costButton.transform.GetChild(0).GetComponent<Text>().text = specialAttack.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }

        // Check level Req
        if (specialAttack.LevelRequirement > Profile.current.currentXPlevel) {
            window_LockGO.SetActive(true);
            window_LevelLock.text = "Level " + specialAttack.LevelRequirement.ToString();
            costButton.interactable = false;
        } else {
            window_LockGO.SetActive(false);
        }
    }

    void Update() {
        if (Profile.current.equippedSpecialAttack.Equals(refspecialAttack.ID))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        }
        else if (Profile.current.ownedSpecialAttacks.Contains(refspecialAttack.TypeOfSpecialAttack))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        }
        else
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = refspecialAttack.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }
    }

    public void TryToBuyOrEquipSpecialAttack() {
        if (Profile.current.ownedSpecialAttacks.Contains(refspecialAttack.TypeOfSpecialAttack) && Profile.current.equippedSpecialAttack != refspecialAttack.TypeOfSpecialAttack)
        {
            Profile.current.equippedSpecialAttack = refspecialAttack.TypeOfSpecialAttack;
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;
        }
        else {
            shopMan.BuySpecialAttack(refspecialAttack);
        }
    }
}
