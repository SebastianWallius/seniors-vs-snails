﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WinLoseScreen : MonoBehaviour {
    
    // A class for sorting and keeping track of which snails and flowers that existed in the level
    public class DisplayItem {
        public GameObject refPrefab;
        public Sprite itemImage;
        public int itemCount;
        public int itemReward;
        public string rewardSuffix;

        public DisplayItem(GameObject obj, int reward, Sprite image, string suffix) {
            refPrefab = obj;
            itemImage = image;
            itemCount = 1;
            itemReward = reward;
            rewardSuffix = suffix;
        }
    }
    
    // Items to display:
    [HideInInspector] public List<DisplayItem> flowersToDisplay = new List<DisplayItem>();
    [HideInInspector] public List<DisplayItem> snailsToDisplay = new List<DisplayItem>();
    [HideInInspector] public List<DisplayItem> seedsToDisplay = new List<DisplayItem>();
    [HideInInspector] public List<DisplayItem> moneyToDisplay = new List<DisplayItem>();

    // Reference to row displays:
    [HideInInspector] public List<GameObject> statsRows = new List<GameObject>();

    // Internal references
    [SerializeField] private Slider xpSlider;
    [SerializeField] private Text levelText;
    [SerializeField] private Transform scrollWindowContent;
    [SerializeField] private GameObject rewardRow_prefab;
    [HideInInspector] public bool animateReward = false;
    private int playerLevelbeforeReward;

    public void LoadWinScreenUI(int playerCurrentXP, int playerLvl)
    {
        // XP text and slider animation preperation
        animateReward = true;
        playerLevelbeforeReward = playerLvl;
        levelText.text = "Level " + playerLevelbeforeReward;
        xpSlider.minValue = (XPManager.XPtoLevel(playerLevelbeforeReward));
        xpSlider.maxValue = (XPManager.XPtoLevel(playerLevelbeforeReward + 1));
        xpSlider.value = playerCurrentXP;


        CreateRows(flowersToDisplay);
        CreateRows(snailsToDisplay);
        CreateRows(seedsToDisplay);
        CreateRows(moneyToDisplay);
        scrollWindowContent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, (statsRows.Count * 150f));
    }

    void Update() {
        if (animateReward) {
            xpSlider.value = Mathf.Lerp(xpSlider.value * 1.0f, Profile.current.currentXP*1.0f, 0.1f);

            // If we leveled up!
            if ((Profile.current.currentXPlevel > playerLevelbeforeReward) && xpSlider.value >= xpSlider.maxValue) {
                playerLevelbeforeReward++;
                levelText.text = "Level " + playerLevelbeforeReward;
                xpSlider.minValue = (XPManager.XPtoLevel(playerLevelbeforeReward));
                xpSlider.maxValue = (XPManager.XPtoLevel(playerLevelbeforeReward + 1));
                xpSlider.value = xpSlider.minValue;
            }
        }
    }

    void CreateRows(List<DisplayItem> itemsToDisplay) {
        foreach (DisplayItem item in itemsToDisplay)
        {
            GameObject row = (GameObject)Instantiate(rewardRow_prefab, scrollWindowContent.position, Quaternion.identity);
            row.transform.SetParent(scrollWindowContent);
            row.GetComponent<RectTransform>().localScale = Vector3.one;


            if (statsRows.Count == 0)
                row.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(0f, 0f);
            else
                row.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(0f, -(statsRows.Count * 150f)); ;

            row.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, 150f);
            row.GetComponent<WinStatsRowUI>().DisplayItem(item);
            statsRows.Add(row);
        }
    }

    // DISPLAY LISTS

    public void AddFlowerToDisplayList(GameObject flower) {
        if (flowersToDisplay.Count > 0) {
            bool alreadyInList = false;
            foreach (DisplayItem item in flowersToDisplay)
            {
                if (item.refPrefab.GetComponent<IFlower>().TypeOfFlower == flower.GetComponent<IFlower>().TypeOfFlower)
                {
                    alreadyInList = true;
                    item.itemCount++;
                    break;
                }
            }

            if(!alreadyInList)
                flowersToDisplay.Add(new DisplayItem(flower, flower.GetComponent<IFlower>().SurviveReward,
                Resources.Load(flower.GetComponent<IFlower>().StandardSpriteName, typeof(Sprite)) as Sprite, "$"));
        }
        else
        {
            flowersToDisplay.Add(new DisplayItem(flower, flower.GetComponent<IFlower>().SurviveReward, 
                Resources.Load(flower.GetComponent<IFlower>().StandardSpriteName, typeof(Sprite)) as Sprite, "$"));
        }
    }

    public void RemoveFlowerFromDisplayList(GameObject flower) {
        foreach (DisplayItem item in flowersToDisplay)
        {
            if (item.refPrefab.GetComponent<IFlower>().TypeOfFlower == flower.GetComponent<IFlower>().TypeOfFlower)
            {
                item.itemCount--;
                break;
            }
        }
    }

    public void AddSnailToDisplayList(GameObject snail) {
        
        if (snailsToDisplay.Count > 0)
        {
            bool alreadyInList = false;
            foreach (DisplayItem item in snailsToDisplay)
            {
                if (item.refPrefab.GetComponent<ISnail>().TypeOfSnail == snail.GetComponent<ISnail>().TypeOfSnail)
                {
                    alreadyInList = true;
                    item.itemCount++;
                    break;
                }
            }

            if (!alreadyInList)
                snailsToDisplay.Add(new DisplayItem(snail, snail.GetComponent<ISnail>().XpGain, 
                    Resources.Load(snail.GetComponent<ISnail>().StandardSpriteName, typeof(Sprite)) as Sprite, "XP"));
        }
        else
        {
            snailsToDisplay.Add(new DisplayItem(snail, snail.GetComponent<ISnail>().XpGain,
                    Resources.Load(snail.GetComponent<ISnail>().StandardSpriteName, typeof(Sprite)) as Sprite, "XP"));
        }
    }

    public void AddSeedToDisplayList(SeedData.SeedType seedType)
    {
        if (seedsToDisplay.Count > 0)
        {
            bool alreadyInList = false;
            foreach (DisplayItem item in seedsToDisplay)
            {
                if (item.refPrefab.GetComponent<ISeed>().TypeOfSeed == seedType)
                {
                    alreadyInList = true;
                    item.itemCount++;
                    break;
                }
            }

            if (!alreadyInList)
                seedsToDisplay.Add(new DisplayItem(ItemsManager.instance.seedType_to_GO[seedType], 1,
                    Resources.Load(ItemsManager.instance.seedType_to_GO[seedType].GetComponent<ISeed>().SeedImageName, typeof(Sprite)) as Sprite, ""));
        }
        else
        {
            seedsToDisplay.Add(new DisplayItem(ItemsManager.instance.seedType_to_GO[seedType], 1,
                                Resources.Load(ItemsManager.instance.seedType_to_GO[seedType].GetComponent<ISeed>().SeedImageName, typeof(Sprite)) as Sprite, ""));
        }
    }

    public void AddMoneyToDisplayList(GameObject money)
    {
        if (moneyToDisplay.Count > 0)
        {
            bool alreadyInList = false;
            foreach (DisplayItem item in moneyToDisplay)
            {
                if (item.refPrefab.GetComponent<Money>().moneyAmount == money.GetComponent<Money>().moneyAmount)
                {
                    alreadyInList = true;
                    item.itemCount++;
                    break;
                }
            }

            if (!alreadyInList)
                seedsToDisplay.Add(new DisplayItem(money, money.GetComponent<Money>().moneyAmount,
                    Resources.Load(money.GetComponent<Money>().graphics.GetComponent<SpriteRenderer>().sprite.name, typeof(Sprite)) as Sprite, ""));
        }
        else
        {
            seedsToDisplay.Add(new DisplayItem(money, money.GetComponent<Money>().moneyAmount,
                Resources.Load(money.GetComponent<Money>().graphics.GetComponent<SpriteRenderer>().sprite.name, typeof(Sprite)) as Sprite, ""));
        }
    }



    public void ContinueButton() {
        GameObject.Find("_LevelManager").GetComponent<LevelManager>().LoadNextLevel();
    }

    public void RestartButton() {
        GameManager.instance.RestartScene();
    }

    public void MainMenuButton() {
        GameManager.instance.ChangeScene("MainMenu");
    }

    public void ShopButton() {
        GameManager.instance.ChangeScene("ShopMode");
    }
}
