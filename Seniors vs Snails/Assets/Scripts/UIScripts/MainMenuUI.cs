﻿using UnityEngine;
using System.Collections;

public class MainMenuUI : MonoBehaviour {

    [SerializeField] private GameObject settingsWindow;


    void Start()
    {
        settingsWindow.SetActive(false);

        // Music
        if (SoundManager.instance.audS_Music.clip == null || SoundManager.instance.audS_Music.clip != SoundManager.instance.mainMenuMusic)
            SoundManager.instance.PlayMusic(SoundManager.instance.mainMenuMusic);
    }



    public void ShowSettingsWindow() {
        settingsWindow.SetActive(true);
    }

    public void HideSettingsWindow() {
        settingsWindow.SetActive(false);
    }
}
