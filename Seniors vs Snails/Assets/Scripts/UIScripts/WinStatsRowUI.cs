﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinStatsRowUI : MonoBehaviour {

    [SerializeField] private Image row_ItemImage;
    [SerializeField] private Text row_ItemCount;
    [SerializeField] private Text row_ItemReward;

    public void DisplayItem(WinLoseScreen.DisplayItem item) {
        row_ItemImage.sprite = item.itemImage;
        row_ItemImage.preserveAspect = true;
        row_ItemCount.text = item.itemCount.ToString();

        row_ItemReward.text = (item.itemReward * item.itemCount).ToString() + item.rewardSuffix;
    }
}
