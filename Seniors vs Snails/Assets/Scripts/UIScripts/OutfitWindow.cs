﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OutfitWindow : MonoBehaviour {

    private Outfit refOutfit;
    [HideInInspector] public ShopManager shopMan;

    public Text window_OutfitName;
    public Image window_OutfitImage;
    public Text window_OutfitType;
    public Text window_Description;
    public Button costButton;
    public GameObject window_LockGO;
    public Text window_LevelLock;


    // Displays given Outfit
    public void DisplayOutfit(Outfit outfit) {
        refOutfit = outfit;
        window_OutfitName.text = outfit.Name;
        window_OutfitImage.sprite = Resources.Load(outfit.ImageName, typeof(Sprite)) as Sprite;
        window_OutfitType.text = outfit.TypeOfOutfit.ToString();
        window_Description.text = outfit.Description;


        // Check Ownership / Cost
        if (Profile.current.CheckIfEquippedOutfit(outfit)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        } else if (Profile.current.ownedOutfits.Contains(outfit.ID)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        } else {
            costButton.transform.GetChild(0).GetComponent<Text>().text = outfit.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }

        // Check level Req
        if (outfit.LevelRequirement > Profile.current.currentXPlevel) {
            window_LockGO.SetActive(true);
            window_LevelLock.text = "Level " + outfit.LevelRequirement.ToString();
            costButton.interactable = false;
        } else {
            window_LockGO.SetActive(false);
        }
    }

    void Update() {
        if (Profile.current.CheckIfEquippedOutfit(refOutfit))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        }
        else if (Profile.current.ownedOutfits.Contains(refOutfit.ID))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        }
        else
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = refOutfit.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }
    }

    public void TryToBuyOrEquipOutfit() {
        if (Profile.current.ownedOutfits.Contains(refOutfit.ID) && !Profile.current.CheckIfEquippedOutfit(refOutfit))
        {
            Profile.current.EquipOutfit(refOutfit);
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;
        }
        else {
            shopMan.BuyOutfit(refOutfit);
        }
    }
}
