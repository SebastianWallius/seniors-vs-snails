﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelUpdate : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "Level " + Profile.current.currentXPlevel;
	}
}
