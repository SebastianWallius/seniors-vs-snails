﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponWindow : MonoBehaviour {

    private IWeapon refWeapon;
    [HideInInspector] public ShopManager shopMan;

    public Text window_WeaponName;
    public Image window_WeaponImage;
    public Slider window_AttackSlider;
    public Slider window_SwiftnessSlider;
    public Slider window_WeightSlider;
    public Text window_WeaponType;
    public Text window_Description;
    public Button costButton;
    public GameObject window_LockGO;
    public Text window_LevelLock;


    // Displays given weapon
    public void DisplayWeapon(IWeapon w) {
        refWeapon = w;
        window_WeaponName.text = w.WeaponName;
        window_WeaponImage.sprite = Resources.Load(w.ImageName, typeof(Sprite)) as Sprite;
        window_AttackSlider.value = w.Damage;
        window_SwiftnessSlider.value = w.Swiftness;
        window_WeightSlider.value = w.Weight;
        window_WeaponType.text = w.TypeOfWeapon.ToString();
        window_Description.text = w.Description;


        // Check Ownership / Cost
        if (Profile.current.equippedWeapon.Equals(w.Weapon)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        } else if (Profile.current.ownedWeapons.Contains(w.Weapon)) {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        } else {
            costButton.transform.GetChild(0).GetComponent<Text>().text = w.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }

        // Check level Req
        if (w.LevelRequirement > Profile.current.currentXPlevel) {
            window_LockGO.SetActive(true);
            window_LevelLock.text = "Level " + w.LevelRequirement.ToString();
            costButton.interactable = false;
        } else {
            window_LockGO.SetActive(false);
        }
    }

    void Update() {
        if (Profile.current.equippedWeapon.Equals(refWeapon.Weapon))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;

        }
        else if (Profile.current.ownedWeapons.Contains(refWeapon.Weapon))
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Owned";
            costButton.GetComponent<Image>().color = Color.blue;
        }
        else
        {
            costButton.transform.GetChild(0).GetComponent<Text>().text = refWeapon.Cost.ToString() + "$";
            costButton.GetComponent<Image>().color = Color.red;
        }
    }

    public void TryToBuyOrEquippWeapon() {
        if (Profile.current.ownedWeapons.Contains(refWeapon.Weapon) && Profile.current.equippedWeapon != refWeapon.Weapon)
        {
            Profile.current.equippedWeapon = refWeapon.Weapon;
            costButton.transform.GetChild(0).GetComponent<Text>().text = "Equipped";
            costButton.GetComponent<Image>().color = Color.green;
        }
        else {
            shopMan.BuyWeapon(refWeapon);
        }
    }
}
