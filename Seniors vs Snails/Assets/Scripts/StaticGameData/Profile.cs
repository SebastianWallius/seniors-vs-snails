﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Profile {

    [System.Serializable]
    public class GardenModePlantInfo {

        public SeedData.SeedType seedType;
        public DateTime plantTime;


        public GardenModePlantInfo(SeedData.SeedType type, DateTime time) {
            seedType = type;
            plantTime = time;
        }
    }

    public static Profile current;

    public string profileName = "";

    // Player attributes
    public int currentWorldLevel = 1;
    public int money = 0;
    public int currentXPlevel = 1;
    public int currentXP = 0;

    // Player inventory and equipped items
    public WeaponData.Weapon equippedWeapon;
    public SpecialAttacksData.SpecialAttackType equippedSpecialAttack;

    public int equippedOutfit_Hat;
    public int equippedOutfit_Dress;
    public int equippedOutfit_Shoes;


    public List<WeaponData.Weapon> ownedWeapons;
    public List<SpecialAttacksData.SpecialAttackType> ownedSpecialAttacks;
    public List<int> ownedOutfits;
    public Dictionary<SeedData.SeedType, int> ownedSeeds;
    public List<SeedData.SeedType> encounteredSeeds; // For knowing which seeds the player has planted, and know info about



    // Garden Upgrades
    public Dictionary<int, GardenModePlantInfo> plantAtPos;




    public Profile(string name) {
        current = this;
        this.profileName = name;

        ownedWeapons = new List<WeaponData.Weapon>();
        ownedSpecialAttacks = new List<SpecialAttacksData.SpecialAttackType>();
        ownedOutfits = new List<int>();
        ownedSeeds = new Dictionary<SeedData.SeedType, int>();
        encounteredSeeds = new List<SeedData.SeedType>();

        // Garden mode plants:
        plantAtPos = new Dictionary<int, GardenModePlantInfo>();

        // Add the start weapon, Hands
        ownedWeapons.Add(WeaponData.Weapon.Hands);
        equippedWeapon = WeaponData.Weapon.Hands;
        equippedSpecialAttack = SpecialAttacksData.SpecialAttackType.None;
        



        // IF TEST
        if (name == "test" || name == "Test") {
            currentWorldLevel = 8;
            currentXPlevel = 50;
            currentXP = XPManager.XPtoLevel(50);
            money = 10000;

            ownedSeeds.Add(SeedData.SeedType.Basic, 50);
            //equippedSpecialAttack = SpecialAttacksData.SpecialAttackType.Boule;
        }


        // Add a start outfit, the first index
        //ownedOutfits.Add(ItemsManager.instance.outfits[0].GetComponent<Outfit>().ID);
        //ownedOutfits.Add(ItemsManager.instance.outfits[1].GetComponent<Outfit>().ID);
        //equippedOutfit_Dress = ownedOutfits[0];
        //equippedOutfit_Shoes = ownedOutfits[1];
    }

    public void StoreInfoAboutPlant(SeedData.SeedType type, int pos) {
        plantAtPos.Add(pos, new GardenModePlantInfo(type, DateTime.Now));
    }

    public void EquipOutfit(Outfit outfit) {
        if (outfit.TypeOfOutfit == Outfit.OutfitType.Hat) {
            equippedOutfit_Hat = outfit.ID;
        }
        else if (outfit.TypeOfOutfit == Outfit.OutfitType.Dress)
        {
            equippedOutfit_Dress = outfit.ID;
        }
        else if (outfit.TypeOfOutfit == Outfit.OutfitType.Shoes)
        {
            equippedOutfit_Shoes = outfit.ID;
        }
    }

    public bool CheckIfEquippedOutfit(Outfit outfit) {
        if (outfit.TypeOfOutfit == Outfit.OutfitType.Hat && outfit.ID == equippedOutfit_Hat)
        {
            return true;
        }
        else if (outfit.TypeOfOutfit == Outfit.OutfitType.Dress && outfit.ID == equippedOutfit_Dress)
        {
            return true;
        }
        else if (outfit.TypeOfOutfit == Outfit.OutfitType.Shoes && outfit.ID == equippedOutfit_Shoes)
        {
            return true;
        }

        return false;
    }

    public void SetCurrentProfile() {
        current = this;
    }

    public void ResetProfile() {
        current = null;
    }

    public void AddXP(int xp) {
        currentXP += xp;

        // If we have leveled up
        while(currentXP >= XPManager.XPtoLevel(currentXPlevel + 1)) {
            currentXPlevel++;
        }
    }

}
