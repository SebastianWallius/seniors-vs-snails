﻿using UnityEngine;
using System.Collections;

public static class XPManager {

    // This script manage the xp to each level using a formula (the xp limit is not defined!)

    // The formula is: 
    // (Base * level) + (an increase modifier) * (Base * level)

    private static int baseXP = 100;
    private static float increaseModifier = 0.1f; // (10%)


    public static int XPtoLevel(int level) {
        return ((baseXP * (level-1)) + Mathf.RoundToInt((baseXP * (level-1))*increaseModifier));
    }

}
