﻿using System.Collections.Generic;

public static class TutorialManager {

    // This class handles the tutrorials in the game

    public enum TutorialInfo {
        None,
        Introduction,
        MaterialSpeeds,
        StaticBlockingObjects,
        FlowerCactus,
        SpecialAttackBoule,
        Area8Boss
    }

    public static string[] tutorialText_Introduction = {
        "Terrific, you're here!",
        "The snails have begun an invasion of our garden and we need to stop them!",
        "Don't let them eat our flowers, because then we can't sell em' and get that sweet cash. Time to defend!"
    };

    public static string[] tutorialText_MaterialSpeeds = {
        "Different materials have different attributes, you walk fast on gravel but slower on dirt.",
        "The opposite goes for the snails. They go faster on dirt than gravel."
    };

    public static string[] tutorialText_StaticBlockingObjects = {
        "Some objects block the path for both you and snails.",
        "Be aware that these objects can create areas that you can defend more easily.",
        "But, they can also block out of vital defending areas. Making it harder."
    };

    public static string[] tutorialText_FlowerCactus = {
        "That right there is a Cactus. It damages snails when they attack. Pretty tough bastard ey?",
        "These flowers make it harder for snails to bring down your garden."
    };

    public static string[] tutorialText_SpecialAttackBoule = {
        "Wow, you got a boule ball! Your very first special attack!",
        "You can use it by filling the special attack meter beside your attack button.",
        "The meter is filled when you kill snails, so keep up the snail onslaught.",
        "When you activate the special attack, you will throw your boule ball forward. When it lands it will explode, damaging and stunning nearby snails."
    };

    public static string[] tutorial_Area8Boss = {
        "Hmm, do you hear that?",
        "Either it's my belly \n or a giant boss snail,\n might be a combination of them both actually..."
    };

    public static Dictionary<TutorialInfo, string[]> tutorialInfoToTexts = new Dictionary<TutorialInfo, string[]>(){
        { TutorialInfo.Introduction, tutorialText_Introduction},
        { TutorialInfo.MaterialSpeeds, tutorialText_MaterialSpeeds},
        { TutorialInfo.StaticBlockingObjects, tutorialText_StaticBlockingObjects},
        { TutorialInfo.FlowerCactus, tutorialText_FlowerCactus},
        { TutorialInfo.SpecialAttackBoule, tutorialText_SpecialAttackBoule},
        { TutorialInfo.Area8Boss, tutorial_Area8Boss}
    };
}
