﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemsManager : MonoBehaviour{

    public static ItemsManager instance = null;

    // All items in the game:
    public GameObject[] weapons;
    public GameObject[] specialAttack;
    public GameObject[] outfits;
    public GameObject[] seeds;
    public GameObject[] staticObjects;
    public GameObject[] flowers;
    public GameObject[] snails;


    // Conversion dictionaries
    public Dictionary<WeaponData.Weapon, GameObject> weapon_to_GO = new Dictionary<WeaponData.Weapon, GameObject>();
    public Dictionary<SpecialAttacksData.SpecialAttackType, GameObject> specialAttackType_to_GO = new Dictionary<SpecialAttacksData.SpecialAttackType, GameObject>();
    public Dictionary<int, GameObject> outfits_ID_to_GO = new Dictionary<int, GameObject>();
    public Dictionary<SeedData.SeedType, GameObject> seedType_to_GO = new Dictionary<SeedData.SeedType, GameObject>();
    public Dictionary<StaticObjectsData.StaticObjectType, GameObject> staticObjectType_to_GO = new Dictionary<StaticObjectsData.StaticObjectType, GameObject>();
    public Dictionary<FlowersData.FlowerType, GameObject> flowerType_to_GO = new Dictionary<FlowersData.FlowerType, GameObject>();
    public Dictionary<SnailsData.SnailType, GameObject> snailType_to_GO = new Dictionary<SnailsData.SnailType, GameObject>();





    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        
        LoadItemsGO();
    }

    void LoadItemsGO() {
        foreach (GameObject we_GO in weapons) {
            if(we_GO != null)
                weapon_to_GO.Add(we_GO.GetComponent<IWeapon>().Weapon, we_GO);
        }

        foreach (GameObject sa_GO in specialAttack)
        {
            specialAttackType_to_GO.Add(sa_GO.GetComponent<ISpecialAttack>().TypeOfSpecialAttack, sa_GO);
        }

        foreach (GameObject out_GO in outfits)
        {
            outfits_ID_to_GO.Add(out_GO.GetComponent<Outfit>().ID, out_GO);
        }

        foreach (GameObject seed_GO in seeds)
        {
            seedType_to_GO.Add(seed_GO.GetComponent<ISeed>().TypeOfSeed, seed_GO);
        }

        foreach (GameObject staticObject_GO in staticObjects)
        {
            staticObjectType_to_GO.Add(staticObject_GO.GetComponent<StaticObject>().objectType, staticObject_GO);
        }

        foreach (GameObject flower_GO in flowers)
        {
            flowerType_to_GO.Add(flower_GO.GetComponent<IFlower>().TypeOfFlower, flower_GO);
        }

        foreach (GameObject snail_GO in snails)
        {
            snailType_to_GO.Add(snail_GO.GetComponent<ISnail>().TypeOfSnail, snail_GO);
        }
    }
}
