﻿using UnityEngine;

public static class ItemThrowCalculate{

    public static Vector3 CalculateThrowArc(Vector3 playerPos, Vector3 targetpos, float timeToTarget, float itemGravity)
    {
        // A magic formula I stole from teh internetz
        Vector3 toTarget = targetpos - playerPos;
        Vector3 toTargetXZ = toTarget;
        toTargetXZ.y = 0;

        float y = toTarget.y;
        float xz = toTargetXZ.magnitude;

        float t = timeToTarget;
        float v0y = y / t + 0.5f * Physics.gravity.magnitude * t * itemGravity;
        float v0xz = xz / t;

        Vector3 velocity = toTargetXZ.normalized;
        velocity *= v0xz;
        velocity.y = v0y;

        return velocity;
    }
}
