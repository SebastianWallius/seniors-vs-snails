﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EnemyBlock : ScriptableObject{

    [SerializeField]
    public List<EnemyInstance> blockSnails = new List<EnemyInstance>();

    [SerializeField]
    public GameObject[] spawnAreas;

    [SerializeField]
    public int totalSnailsInBlock;

    [SerializeField]
    public enum BlockType {
        Ordered,
        Random,
        Wave
    }

    [SerializeField]
    public BlockType blockType;

    [SerializeField]
    public float spawnInterval;


    public EnemyBlock() {
        spawnAreas = new GameObject[1];
        spawnInterval = 2f;
        blockType = BlockType.Ordered;
    }

    public void AddNewSnailType(GameObject snail) {
        blockSnails.Add(new EnemyInstance(snail, 0));
    }

    public void RemoveSnailType(EnemyInstance enemy) {
        blockSnails.Remove(enemy);
    }

    [System.Serializable]
    public class EnemyInstance {

        [SerializeField]
        public GameObject prefab;

        [SerializeField]
        public int count;


        public EnemyInstance(GameObject obj, int i) {
            prefab = obj;
            count = i;
        }
    }

}
