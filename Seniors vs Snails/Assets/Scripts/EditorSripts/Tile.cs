﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;

[ExecuteInEditMode]
public class Tile : MonoBehaviour {

    public TileManager.TileType tileType;
    private TileManager.TileType currTileType;
    private string[] matTags;



# if UNITY_EDITOR
    void Update() {

        if (tileType != currTileType) {
            UpdateVisuals();
        }
    }
# endif

    void UpdateVisuals() {
        GetComponent<SpriteRenderer>().sprite = Resources.Load(TileManager.tileTypeToSprite[tileType], typeof(Sprite)) as Sprite;
        gameObject.tag = TileManager.tileTypeToMatTag[tileType];




        // Special check if water:
        if (tileType == TileManager.TileType.Water)
        {
            // If water, then we need to make collider not a trigger and add a polyNavObstacle on it

            GetComponent<Collider2D>().isTrigger = false;

            if (gameObject.GetComponent<PolyNavObstacle>() == null)
            {
                gameObject.AddComponent<PolyNavObstacle>().extraOffset = 1.2f;
            }

        }
        else {
            GetComponent<Collider2D>().isTrigger = true;

            if (gameObject.GetComponent<PolyNavObstacle>() != null)
            {
                DestroyPolyNavRecursively();
            }
        }

        currTileType = tileType;
    }

    void DestroyPolyNavRecursively() {
        DestroyImmediate(gameObject.GetComponent<PolyNavObstacle>());

        if (gameObject.GetComponent<PolyNavObstacle>() != null) {
            DestroyPolyNavRecursively();
        }
    }

}
