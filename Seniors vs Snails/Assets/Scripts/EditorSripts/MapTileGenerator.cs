﻿using UnityEngine;
using System.Collections;
using System.Linq;

[ExecuteInEditMode]
public class MapTileGenerator : MonoBehaviour {

    public static MapTileGenerator instance;
    public Transform tileParent;
    public Transform staticObjectsParent;
    public Transform boundaryParent;
    public PolyNav2D polynav;
    public int tiles_x;
    public int tiles_y;

    public int currentTiles_x;
    public int currentTiles_y;

    void Awake() {
        instance = this;
    }

    public void CreateMapTiles() {
        RemoveAllCurrentTilesAndBoundaries();
        currentTiles_x = tiles_x;
        currentTiles_y = tiles_y;

        // We limit values
        if (tiles_x < 13)
            tiles_x = 13;
        else if (tiles_x > 50)
            tiles_x = 50;

        if (tiles_y < 7)
            tiles_y = 7;
        else if (tiles_y > 50)
            tiles_y = 50;


        // We calculate our map middlepoint
        int startPosX = 0;
        int startPosY = 0;
        bool xIsEven = (tiles_x % 2 == 0);
        bool yIsEven = (tiles_y % 2 == 0);

        if (xIsEven) {
            startPosX = -(tiles_x - 2);
        }
        else {
            startPosX = -(tiles_x - 1);
        }

        if (yIsEven)
        {
            startPosY = (tiles_y - 2);
        }
        else
        {
            startPosY = (tiles_y - 1);
        }


        // We create the tiles in a loop
        int number = 1;
        int posX = startPosX;
        int posY = startPosY;

        for (int x = 0; x < tiles_x; x++) {
            posY = startPosY;
            for (int y = 0; y < tiles_y; y++) {
                // Create gameobject
                GameObject tile = new GameObject("Tile_" + number);
                tile.transform.parent = tileParent;
                tile.transform.position = new Vector3(posX, posY, 0f);

                // Sprite
                tile.AddComponent<SpriteRenderer>();
                tile.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Floor");
                tile.GetComponent<SpriteRenderer>().sortingOrder = -100;
                tile.GetComponent<SpriteRenderer>().sprite = Resources.Load("Grass_sprite", typeof(Sprite)) as Sprite;

                // Collider
                tile.AddComponent<BoxCollider2D>();
                tile.GetComponent<BoxCollider2D>().isTrigger = true;
                tile.GetComponent<BoxCollider2D>().size = new Vector2(2f, 2f);

                // Script
                tile.AddComponent<Tile>();
                tile.GetComponent<Tile>().tileType = TileManager.TileType.Grass;
                tile.tag = "BgMat_Grass";
                tile.layer = 14;

                number++;
                posY = posY - 2;
            }
            posX = posX + 2;
        }

        // Boundery 1
        GameObject boundery_1 = new GameObject("Boundery_1");
        boundery_1.transform.parent = boundaryParent;
        boundery_1.transform.position = new Vector2(0f, startPosY+1);
        boundery_1.AddComponent<BoxCollider2D>().size = new Vector2(tiles_x*2, 0.5f);


        // Boundery 2
        GameObject boundery_2 = new GameObject("Boundery_2");
        boundery_2.transform.parent = boundaryParent;
        boundery_2.transform.position = new Vector2(0f, -startPosY - 1);
        boundery_2.AddComponent<BoxCollider2D>().size = new Vector2(tiles_x * 2, 0.5f);




        // Boundery 3
        GameObject boundery_3 = new GameObject("Boundery_3");
        boundery_3.transform.parent = boundaryParent;
        boundery_3.transform.position = new Vector2(startPosX - 1, 0f);
        boundery_3.AddComponent<BoxCollider2D>().size = new Vector2(0.5f, tiles_y * 2);


        // Boundery 4
        GameObject boundery_4 = new GameObject("Boundery_4");
        boundery_4.transform.parent = boundaryParent;
        boundery_4.transform.position = new Vector2(-startPosX + 1, 0f);
        boundery_4.AddComponent<BoxCollider2D>().size = new Vector2(0.5f, tiles_y * 2);

        // Corrected Placements
        if (yIsEven)
        {
            boundery_2.transform.position = new Vector2(0f, -startPosY - 3);
            boundery_3.GetComponent<BoxCollider2D>().offset = new Vector2(0f, -1f);
            boundery_4.GetComponent<BoxCollider2D>().offset = new Vector2(0f, -1f);
        }

        if (xIsEven)
        {
            boundery_1.GetComponent<BoxCollider2D>().offset = new Vector2(1f, 0f);
            boundery_2.GetComponent<BoxCollider2D>().offset = new Vector2(1f, 0f);
            boundery_4.transform.position = new Vector2(-startPosX + 3, 0f);
        }


        // PolyNav
        Vector2 point1 = new Vector2(startPosX - 1, startPosY + 1);
        Vector2 point2 = new Vector2(-startPosX + 1, startPosY + 1);
        Vector2 point3 = new Vector2(-startPosX + 1, -startPosY - 1);
        Vector2 point4 = new Vector2(startPosX - 1, -startPosY - 1);

        if (xIsEven) {
            point2 = new Vector2(-startPosX + 3, startPosY + 1);
            point3 = new Vector2(-startPosX + 3, -startPosY - 1);
        }
        if (yIsEven) {
            point3 = new Vector2(-startPosX + 1, -startPosY - 3);
            point4 = new Vector2(startPosX - 1, -startPosY - 3);
        }

        if(yIsEven && xIsEven)
            point3 = new Vector2(-startPosX + 3, -startPosY - 3);


        Vector2[] points = new Vector2[4] {
            point1,
            point2,
            point3,
            point4};

        polynav.gameObject.GetComponent<PolygonCollider2D>().SetPath(0, points);

    }

    public void RemoveAllCurrentTilesAndBoundaries() {
        var tempTileList = tileParent.Cast<Transform>().ToList();
        var tempBoundaryList = boundaryParent.Cast<Transform>().ToList();

        foreach (Transform child in tempTileList) {
            DestroyImmediate(child.gameObject);
        }

        foreach (Transform child in tempBoundaryList)
        {
            DestroyImmediate(child.gameObject);
        }
    }

    public TileManager.TileType[] GetAllTileTypesInOrder() {
        TileManager.TileType[] tileTypes = new TileManager.TileType[currentTiles_x * currentTiles_y];
        int i = 0;
        foreach (Transform tile in tileParent) {
            tileTypes[i] = tile.GetComponent<Tile>().tileType;
            i++;
        }
        return tileTypes;
    }

    public StaticObjectsData.StaticObjectType[] GetAllStaticObjectsType()
    {
        StaticObjectsData.StaticObjectType[] objectTypes = new StaticObjectsData.StaticObjectType[staticObjectsParent.childCount];
        int i = 0;
        foreach (Transform staticObject in staticObjectsParent)
        {
            objectTypes[i] = staticObject.GetComponent<StaticObject>().objectType;
            i++;
        }
        return objectTypes;
    }

    public Transform[] GetAllStaticObjectsPositions()
    {
        Transform[] objectPos = new Transform[staticObjectsParent.childCount];
        int i = 0;
        foreach (Transform staticObject in staticObjectsParent)
        {
            objectPos[i] = staticObject;
            i++;
        }
        return objectPos;
    }
}
