﻿public static class LevelCreation {

    // This class converts a scene to a LevelInfo-object and can also take a LevelInfo-object and
    // convert it into a playable level.

    public static LevelInfo ConvertCurrentSceneToLevelInfo() {
        return new LevelInfo(
            // Map
            MapTileGenerator.instance.currentTiles_x,
            MapTileGenerator.instance.currentTiles_y,
            MapTileGenerator.instance.GetAllTileTypesInOrder(),

            // Objects
            MapTileGenerator.instance.GetAllStaticObjectsType(),
            MapTileGenerator.instance.GetAllStaticObjectsPositions(),

            // Player
            LevelManager.instance.GetPlayer().transform,
            LevelManager.instance.GetPlayer().GetComponent<Player>().GetEquippedWeapon().GetComponent<IWeapon>(),
            LevelManager.instance.GetPlayer().GetComponent<Player>().GetEquippedSpecialAttack().GetComponent<ISpecialAttack>(),

            // Flower
            LevelManager.instance.GetLevelFlowerType(),
            LevelManager.instance.GetLevelFlowerPos(),

            // Snails
            LevelManager.instance.GetLevelEnemyBlocks()
            );
    }

    public static void ConvertLevelInfoIntoPlayableLevel(LevelInfo level) {

    }
}
