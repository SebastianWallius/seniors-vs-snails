﻿using UnityEngine;
using System.Collections;

public class Weapon_Axe : MonoBehaviour, IWeapon {

    [SerializeField] private string weaponName = "Axe";
    public string WeaponName { get { return weaponName; } }

    [SerializeField] private string description = "Don't be too careful";
    public string Description { get { return description; } }

    public string ImageName { get { return WeaponData.GetStandardSpriteName(weapon); } }

    private WeaponData.Weapon weapon = WeaponData.Weapon.Axe;
    public WeaponData.Weapon Weapon { get { return weapon;  } }

    [SerializeField] private WeaponData.WeaponType weaponType = WeaponData.WeaponType.Melee;
    public WeaponData.WeaponType TypeOfWeapon { get { return weaponType; } }

    [SerializeField] private WeaponData.AnimationType animationType = WeaponData.AnimationType.Melee_Heavy;
    public WeaponData.AnimationType TypeOfAnimation { get { return animationType; } }

    [SerializeField] private float damage = 5f;
    public float Damage { get { return damage; } }

    [SerializeField] private float swiftness = 1f;
    public float Swiftness { get { return swiftness; } }

    [SerializeField] private float weight = 7f;
    public float Weight { get { return weight; } }

    [SerializeField] private int cost = 500;
    public int Cost { get { return cost; } }

    [SerializeField] private int levelReq = 4;
    public int LevelRequirement { get { return levelReq; } }
}
