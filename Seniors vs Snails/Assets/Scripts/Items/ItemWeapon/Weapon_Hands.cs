﻿using UnityEngine;
using System.Collections;

public class Weapon_Hands : MonoBehaviour, IWeapon {

    [SerializeField] private string weaponName = "Hands";
    public string WeaponName { get { return weaponName; } }

    [SerializeField] private string description = "Every senior got a pair";
    public string Description { get { return description; } }

    public string ImageName { get { return WeaponData.GetStandardSpriteName(weapon); } }

    private WeaponData.Weapon weapon = WeaponData.Weapon.Hands;
    public WeaponData.Weapon Weapon { get { return weapon;  } }

    [SerializeField] private WeaponData.WeaponType weaponType = WeaponData.WeaponType.Melee;
    public WeaponData.WeaponType TypeOfWeapon { get { return weaponType; } }

    [SerializeField] private WeaponData.AnimationType animationType = WeaponData.AnimationType.Melee_Quick;
    public WeaponData.AnimationType TypeOfAnimation { get { return animationType; } }

    [SerializeField] private float damage = 2f;
    public float Damage { get { return damage; } }

    [SerializeField] private float swiftness = 5f;
    public float Swiftness { get { return swiftness; } }

    [SerializeField] private float weight = 0f;
    public float Weight { get { return weight; } }

    [SerializeField] private int cost = 0;
    public int Cost { get { return cost; } }

    [SerializeField] private int levelReq = 0;
    public int LevelRequirement { get { return levelReq; } }
}
