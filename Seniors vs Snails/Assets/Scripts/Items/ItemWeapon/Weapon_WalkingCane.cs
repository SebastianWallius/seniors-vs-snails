﻿using UnityEngine;
using System.Collections;

public class Weapon_WalkingCane : MonoBehaviour, IWeapon {

    [SerializeField] private string weaponName = "Walking Cane";
    public string WeaponName { get { return weaponName; } }

    [SerializeField] private string description = "If you need to walk more stable";
    public string Description { get { return description; } }

    public string ImageName { get { return WeaponData.GetStandardSpriteName(weapon); } }

    private WeaponData.Weapon weapon = WeaponData.Weapon.WalkingCane;
    public WeaponData.Weapon Weapon { get { return weapon;  } }

    [SerializeField] private WeaponData.WeaponType weaponType = WeaponData.WeaponType.Melee;
    public WeaponData.WeaponType TypeOfWeapon { get { return weaponType; } }

    [SerializeField] private WeaponData.AnimationType animationType = WeaponData.AnimationType.Melee_Normal;
    public WeaponData.AnimationType TypeOfAnimation { get { return animationType; } }

    [SerializeField] private float damage = 2f;
    public float Damage { get { return damage; } }

    [SerializeField] private float swiftness = 3f;
    public float Swiftness { get { return swiftness; } }

    [SerializeField] private float weight = 2f;
    public float Weight { get { return weight; } }

    [SerializeField] private int cost = 150;
    public int Cost { get { return cost; } }

    [SerializeField] private int levelReq = 1;
    public int LevelRequirement { get { return levelReq; } }
}
