﻿using UnityEngine;
using System.Collections;

public class Special_Boule : MonoBehaviour, ISpecialAttack {

    private string specialAttackName = "Boule Ball";
    public string Name { get { return specialAttackName; } }

    private int specialAttackId = 1;
    public int ID { get { return specialAttackId; } }

    private string description = "A hard hitting throwable ball, that both deal damage and stun snails";
    public string Description { get { return description; } }

    [SerializeField] private int cost = 1000;
    public int Cost { get { return cost; } }

    [SerializeField] private int levelReq = 10;
    public int LevelRequirement { get { return levelReq; } }

    [SerializeField] private int chargeReq = 20;
    public int ChargeRequirement { get { return chargeReq; } }

    private int currentCharge = 0;
    public int CurrentCharge { get { return currentCharge; } set { currentCharge = value; } }

    private string imageName;
    public string ImageName { get { return SpecialAttacksData.GetStandardSpriteName(specialAttackType); } }

    private SpecialAttacksData.SpecialAttackType specialAttackType = SpecialAttacksData.SpecialAttackType.Boule;
    public SpecialAttacksData.SpecialAttackType TypeOfSpecialAttack { get { return specialAttackType; } }

    private SpecialAttacksData.AnimationType animationType = SpecialAttacksData.AnimationType.Special_Boule;
    public SpecialAttacksData.AnimationType TypeOfAnimation { get { return animationType; } }

    [SerializeField] private GameObject bouleBallPrefab;
    public GameObject BouleBallPrefab { get { return bouleBallPrefab; } }

    [SerializeField] private float throwStrength = 2f;
    [SerializeField] private float flyTime = 2f;
    [SerializeField] private float impactRadius = 10f;
    [SerializeField] private float impactDamage = 2f;
    [SerializeField] private float impactStun = 5f;

    public void ActivateSpecialAttack() {
        // Spawn prefab
        Vector3 playerPos = LevelManager.instance.GetPlayerPosition();
        GameObject bouleBall = (GameObject)Instantiate(bouleBallPrefab, playerPos, Quaternion.identity);

        // Calculate landing point
        Vector3 bouleLandingPoint = CalculateLandingPoint(throwStrength);

        // Animate throw


        // Animate ball
        Vector3 velocity = CalculateThrowArc(playerPos, bouleLandingPoint, flyTime);
        bouleBall.GetComponent<Rigidbody2D>().AddForce(velocity, ForceMode2D.Impulse);

        StartCoroutine(WaitForBallFlyTime(bouleBall, flyTime));
    }

    IEnumerator WaitForBallFlyTime(GameObject ball, float flyTime) {
        yield return new WaitForSeconds(flyTime);
        ball.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ball.GetComponent<Animator>().SetTrigger("Impact");
        Vibration.Vibrate(300);

        // Create physics circle
        Collider2D[] impactCircle = Physics2D.OverlapCircleAll(ball.transform.position, impactRadius);
        foreach (Collider2D col in impactCircle) {
            if (col.gameObject.tag == "Enemy") {
                // All snails inside circle take damage, stunned and physics impact hit away from landing point
                col.GetComponent<ISnail>().TakeDamage(impactDamage, ball);
                col.GetComponent<ISnail>().Stun(impactStun);
                col.GetComponent<ISnail>().PhysicsHitImpact(ball.transform.position, 8f);
            }
        }
        StartCoroutine(WaitForDestroyBall(ball, 5f));
    }

    IEnumerator WaitForDestroyBall(GameObject ball, float waitTime) {
        yield return new WaitForSeconds(waitTime);
        Destroy(ball);
    }

    private Vector3 CalculateThrowArc( Vector3 playerPos, Vector3 targetpos, float timeToTarget) {
        // A magic formula I stole from teh internetz
        Vector3 toTarget = targetpos - playerPos;
        Vector3 toTargetXZ = toTarget;
        toTargetXZ.y = 0;

        float y = toTarget.y;
        float xz = toTargetXZ.magnitude;

        float t = timeToTarget;
        float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
        float v0xz = xz / t;
        
        Vector3 velocity = toTargetXZ.normalized;
        velocity *= v0xz;
        velocity.y = v0y;

        return velocity;
    }

    Vector3 CalculateLandingPoint(float throwDist) {
        Player player = LevelManager.instance.GetPlayer().GetComponent<Player>();
        Vector3 landingPoint = Vector3.zero;
        // If player is moving, then throw in the moving direction
        if (player.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 1)
        {
            landingPoint = player.GetComponent<Rigidbody2D>().velocity.normalized * throwDist;
        } else if (player.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude > 0) {
            landingPoint = player.GetComponent<Rigidbody2D>().velocity * throwDist;
        } else {
            // If player is stationary, then throw in the facing direction, but static x-axis
            /*
            if (player.FacingLeft)
            {
                landingPoint = Vector3.left * throwDist;
            } else {
                landingPoint = Vector3.right * throwDist;
            }
            */
            landingPoint = player.GetComponent<Rigidbody2D>().velocity * throwDist;
        }

        // Then we add the player position to the landing point
        landingPoint = player.transform.position + landingPoint;

        // Before we return, we need to check that we don't are outside the map
        if (!Physics2D.OverlapCircle(landingPoint, 0.1f)) {
            landingPoint = CalculateLandingPoint(Mathf.Clamp(throwDist - 0.5f, 0f, throwStrength));
        }

        Debug.DrawLine(player.transform.position, landingPoint, Color.green, 10f);
        return landingPoint;
    }

}
