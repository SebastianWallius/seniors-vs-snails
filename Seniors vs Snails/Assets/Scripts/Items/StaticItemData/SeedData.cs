﻿using System.Collections.Generic;

public static class SeedData {

    public static Dictionary<SeedType, FlowersData.FlowerType> seedTypeToFlowerType = new Dictionary<SeedType, FlowersData.FlowerType>() {
        { SeedType.Basic, FlowersData.FlowerType.Basic },
        { SeedType.Cactus, FlowersData.FlowerType.Cactus},
        { SeedType.Berries, FlowersData.FlowerType.Berries}
    };


    public enum SeedType
    {
        Basic,
        Cactus,
        Berries
    }

    public static string GetStandardSpriteName(SeedType type)
    {
        switch (type)
        {
            case SeedType.Basic:
                return "Seed_Basic";
            case SeedType.Cactus:
                return "Seed_Cactus";
            case SeedType.Berries:
                return "Seed_Berries";
        }
        return "";
    }
}
