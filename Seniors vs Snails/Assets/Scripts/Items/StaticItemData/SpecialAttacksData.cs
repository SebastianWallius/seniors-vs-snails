﻿public static class SpecialAttacksData {

    public enum SpecialAttackType
    {
        None,
        Boule,
        Headbang
    }

    public enum AnimationType
    {
        Melee_Normal,
        Melee_Heavy,
        Melee_Quick,
        Melee_Swing,
        Ranged_Normal,
        Ranged_Trow,
        Ranged_Heavy,
        Special_Boule
    }

    public static string GetStandardSpriteName(SpecialAttackType type)
    {
        switch (type)
        {
            case SpecialAttackType.Boule:
                return "Boule_Ball";
            case SpecialAttackType.Headbang:
                return "Not really a senior attack.png";
        }
        return "";
    }
}
