﻿public static class WeaponData {

    public enum Weapon
    {
        Hands,
        Knife,
        Axe,
        Sword,
        WalkingCane
    }

    public enum WeaponType
    {
        Melee,
        Ranged
    }

    public enum AnimationType
    {
        Melee_Normal,
        Melee_Heavy,
        Melee_Quick,
        Ranged_Normal,
        Ranged_Trow,
        Ranged_Heavy,
        Special_Boule
    }

    public static string GetStandardSpriteName(Weapon type)
    {
        switch (type)
        {
            case Weapon.Hands:
                return "Weapon_Hands";
            case Weapon.Knife:
                return "Weapon_Knife";
            case Weapon.Axe:
                return "Weapon_Axe";
            case Weapon.WalkingCane:
                return "Weapon_WalkingCane";
        }
        return "";
    }

}
