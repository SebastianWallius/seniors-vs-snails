﻿using UnityEngine;
using System.Collections;

public class Money : MonoBehaviour {

    public int moneyAmount;
    [HideInInspector] public GameObject graphics;
    bool pickedUp = false;
    bool canPickUp = false;


    void Start() {
        StartCoroutine(WaitForSpawnAnimation());
    }

    IEnumerator WaitForSpawnAnimation() {
        yield return new WaitForSeconds(1f);
        canPickUp = true;
    }

    public void PickUp() {
        if (!pickedUp) {
            pickedUp = true;
            SoundManager.instance.PlaySoundFx(SoundManager.instance.cash_PickUp);
            //graphics.GetComponent<Animator>().SetBool("PickedUp", true);
            if (GameManager.instance.GetCurrentScene() == "GardenMode")
            {
                Profile.current.money += moneyAmount;
            }
            else {
                RewardManager.instance.StoreMoney(moneyAmount);
            }

            //LevelManager.instance.AddMoneyToStatsRow(this.gameObject);
            Destroy(this.gameObject);
        }
    }


    void OnTriggerStay2D(Collider2D col) {
        if (col.gameObject.tag == "Player" && canPickUp) {
            PickUp();
        }
    }

}
