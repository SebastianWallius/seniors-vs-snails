﻿using UnityEngine;
using System;
using System.Collections;

public class Seed_Basic : MonoBehaviour, ISeed {

    private string seedName = "Seed_Basic";
    public string Name
    {
        get { return seedName; }
    }

    private SeedData.SeedType typeOfSeed = SeedData.SeedType.Basic;
    public SeedData.SeedType TypeOfSeed
    {
        get { return typeOfSeed; }
    }

    public string SeedImageName
    {
        get { return SeedData.GetStandardSpriteName(typeOfSeed); }
    }

    public string FlowerImageName
    {
        get { return FlowersData.GetStandardSpriteName(SeedData.seedTypeToFlowerType[typeOfSeed]); }
    }

    private GardenModePlantArea parentPlantArea;
    public GardenModePlantArea ParentPlantArea
    {
        get { return parentPlantArea; }
        set { parentPlantArea = value; }
    }

    private TimeSpan currentGrowTimeLeft;
    public TimeSpan CurrentGrowTimeLeft
    {
        get { return currentGrowTimeLeft; }
        set { currentGrowTimeLeft = value; }
    }

    [SerializeField] private TimeSpan timeToGrow = TimeSpan.FromMinutes(5);
    public TimeSpan TimeToGrow
    {
        get { return timeToGrow; }
    }

    private DateTime plantTime;
    public DateTime PlantTime
    {
        get { return plantTime; }
        set { plantTime = value; }
    }

    [SerializeField] private int moneyReward = 100;
    public int MoneyReward
    {
        get { return moneyReward; }
    }

    [SerializeField] private bool isDropPrefab;
    public bool IsDropPrefab {
        get { return isDropPrefab; }
        set { isDropPrefab = value; }
    }

    // Instance booleans
    [SerializeField] private GameObject graphics;
    [SerializeField] private GameObject gardenModeFlower;
    [SerializeField] private float flowerMinGrowScale = 0.1f;
    [SerializeField] private float flowerMaxGrowScale = 0.8f;
    private bool canGrow = true;
    private bool displayingFlowerSprite = false;
    private bool canPickUp = false;


    void Start()
    {
        if (isDropPrefab)
            StartCoroutine(WaitForSpawnAnimation());
    }

    void Update() {
        if (!isDropPrefab && canGrow) {
            GrowUpdate();
        }
    }

    IEnumerator WaitForSpawnAnimation() {
        yield return new WaitForSeconds(0.8f);
        canPickUp = true;
    }

    // Updates the grow timer and checks if we have finished growing. ONLY CALLED IN GARDEN MODE
    void GrowUpdate() {
        currentGrowTimeLeft = currentGrowTimeLeft.Subtract(TimeSpan.FromSeconds(Convert.ToDouble(Time.deltaTime)));
        //currentGrowTimeLeft = currentGrowTimeLeft.Subtract(TimeSpan.FromSeconds(Convert.ToDouble(0.5f)));

        if (currentGrowTimeLeft.TotalSeconds <= 0)
        {
            if (!displayingFlowerSprite) {
                SwitchSpritesToFlower();
            }
            canGrow = false;
            FinishedGrowing();

        } else if (!displayingFlowerSprite && (((float)timeToGrow.TotalSeconds*2f) / 3f) > (float)currentGrowTimeLeft.TotalSeconds) {
            SwitchSpritesToFlower();

        } else if (displayingFlowerSprite) {

            // Here we change the flower size to grow with the grow time
            float currentScale = Mathf.Clamp((flowerMaxGrowScale - ((flowerMaxGrowScale - flowerMinGrowScale) / (((2f * (float)timeToGrow.TotalSeconds) / 3f) / (float)currentGrowTimeLeft.TotalSeconds))), flowerMinGrowScale, flowerMaxGrowScale);
            graphics.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        }
    }

    void SwitchSpritesToFlower() {
        displayingFlowerSprite = true;
        GameObject flowerGraphics = (GameObject)Instantiate(gardenModeFlower, transform.position, Quaternion.identity);
        flowerGraphics.transform.parent = graphics.transform.parent;
        Destroy(graphics);
        graphics = flowerGraphics;
        graphics.transform.localScale = new Vector3(flowerMinGrowScale, flowerMinGrowScale, flowerMinGrowScale);
    }

    public void FinishedGrowing() {
        parentPlantArea.PlantIsFinishedGrowing(this);
        graphics.transform.localScale = new Vector3(3f, 3f, 3f);
    }

    void OnTriggerStay2D(Collider2D col) {
        if (isDropPrefab && col.gameObject.tag == "Player" && canPickUp) {
            canPickUp = false;
            LevelManager.instance.AddSeedToStatsRow(typeOfSeed);
            SoundManager.instance.PlaySoundFx(SoundManager.instance.seed_PickUp);
            AddSeedToPlayerInventory();
            Destroy(this.gameObject);
        }
    }


    void AddSeedToPlayerInventory() {
        // We have to check if we own seeds of this type before we add
        if (Profile.current.ownedSeeds.ContainsKey(typeOfSeed)) {
            Profile.current.ownedSeeds[typeOfSeed]++;
        } else {
            Profile.current.ownedSeeds.Add(typeOfSeed, 1);
        }
    }
}
