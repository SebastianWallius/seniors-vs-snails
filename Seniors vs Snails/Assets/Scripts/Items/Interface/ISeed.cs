﻿using UnityEngine;
using System;
public interface ISeed {

    string Name
    {
        get;
    }

    SeedData.SeedType TypeOfSeed {
        get;
    }

    string SeedImageName
    {
        get;
    }

    string FlowerImageName
    {
        get;
    }

    GardenModePlantArea ParentPlantArea {
        get;
        set;
    }

    TimeSpan CurrentGrowTimeLeft
    {
        get;
        set;
    }

    TimeSpan TimeToGrow
    {
        get;
    }

    DateTime PlantTime {
        get;
        set;
    }

    int MoneyReward
    {
        get;
    }

    bool IsDropPrefab {
        get;
        set;
    }

    void FinishedGrowing();
}
