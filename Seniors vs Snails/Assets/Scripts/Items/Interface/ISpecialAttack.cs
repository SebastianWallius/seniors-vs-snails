﻿using UnityEngine;

public interface ISpecialAttack {

    string Name {
        get;
    }

    int ID {
        get;
    }

    string Description {
        get;
    }

    int Cost {
        get;
    }

    int LevelRequirement {
        get;
    }

    int ChargeRequirement {
        get;
    }

    int CurrentCharge {
        get;
        set;
    }
    
    string ImageName {
        get; 
    }

    SpecialAttacksData.SpecialAttackType TypeOfSpecialAttack {
        get;
    }

    SpecialAttacksData.AnimationType TypeOfAnimation {
        get; 
    }


    void ActivateSpecialAttack();
}
