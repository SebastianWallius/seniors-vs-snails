﻿public interface IWeapon {

    string WeaponName {
        get;
    }

    string Description {
        get;
    }

    string ImageName {
        get;
    }

    WeaponData.Weapon Weapon {
        get;
    }

    WeaponData.WeaponType TypeOfWeapon
    {
        get;
    }

    WeaponData.AnimationType TypeOfAnimation
    {
        get;
    }

    float Damage {
        get;
    }

    float Swiftness {
        get;
    }

    float Weight {
        get;
    }

    int Cost {
        get;
    }

    int LevelRequirement {
        get;
    }


}
