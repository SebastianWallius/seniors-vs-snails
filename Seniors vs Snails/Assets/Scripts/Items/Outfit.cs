﻿using UnityEngine;
using System.Collections;

public class Outfit : MonoBehaviour {

    [SerializeField]
    private string outfitName;
    public string Name { get { return outfitName; } }

    [SerializeField]
    private int outfitId;
    public int ID { get { return outfitId; } }

    public enum OutfitType
    {
        Hat,
        Dress,
        Shoes,
    }

    [SerializeField]
    private string description;
    public string Description { get { return description; } }

    [SerializeField]
    private string imageName;
    public string ImageName { get { return imageName; } }

    [SerializeField]
    private OutfitType outfitType;
    public OutfitType TypeOfOutfit { get { return outfitType; } }

    [SerializeField]
    private int cost;
    public int Cost { get { return cost; } }

    [SerializeField]
    private int levelReq;
    public int LevelRequirement { get { return levelReq; } }
}
