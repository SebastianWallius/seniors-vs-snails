﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowManager : MonoBehaviour {

    // This class manages the arrow indicators that an enemy is on the map, but outside the camera view

    [SerializeField] private GameObject arrowPrefab;
    public float mergeAngle = 45f;
    private LevelManager levelMan;

    // Lists for keeping track of units and arrows
    private List<ArrowPointer> arrowsAroundPlayer = new List<ArrowPointer>();
    public Dictionary<GameObject, ArrowPointer> snailToArrow = new Dictionary<GameObject, ArrowPointer>();
        	
	void Start () {
        levelMan = LevelManager.instance;
	}


    void Update () {        
        // Here we create our arrows to point at snails outside our screen
        if (levelMan.GetLevelSnailList().Count > 0) {
            foreach (ISnail snail in levelMan.GetLevelSnailList())
            {
                // We use a unity-function to find out if the snail is outside our screen
                if (!snail.SpriteRenderer.isVisible) 
                {
                    // The snail is not in camera view and we need to create an arrow for it

                    if (arrowsAroundPlayer.Count > 0)
                    {
                        // Before we create a new arrow, we need to check if the angle from the player to the snail
                        // is CLOSE to an another arrow
                        CheckIfCreateNewArrow(snail.SnailGameObject);
                    }
                    else
                    {
                        CreateNewArrow(snail.SnailGameObject);
                    }
                } else {
                    // If the snail is inside our screen, we need to look if it has an arrow pointing at it
                    // If it has, we need to remove the snail from the arrow.
                    if (snailToArrow.ContainsKey(snail.SnailGameObject))
                    {
                        if (snailToArrow[snail.SnailGameObject].objectsToPointAt.Count > 1)
                        {
                            snailToArrow[snail.SnailGameObject].RemoveObjectToPointAt(snail.SnailGameObject);
                        }
                        else
                        {
                            ArrowPointer tempRef = snailToArrow[snail.SnailGameObject];
                            arrowsAroundPlayer.Remove(tempRef);
                            tempRef.RemoveObjectToPointAt(snail.SnailGameObject);
                            snailToArrow.Remove(snail.SnailGameObject);

                            if(tempRef != null)
                                Destroy(tempRef.gameObject);
                        }
                    }
                }
            }

            CheckIfMerge();
        }
	}

    bool CheckIfMerge() {
        // Check if we need to merge two arrows
        if (arrowsAroundPlayer.Count > 1)
        {
            for (int i = 0; i < arrowsAroundPlayer.Count; i++) {
                for (int x = i + 1; x < arrowsAroundPlayer.Count; x++) {
                    if(Mathf.Abs(arrowsAroundPlayer[i].currentAngle - arrowsAroundPlayer[x].currentAngle) < mergeAngle)
                    {
                        MergeArrows(arrowsAroundPlayer[i], arrowsAroundPlayer[x]);
                        CheckIfMerge();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void MergeArrows(ArrowPointer parent, ArrowPointer child) {

        // We need to merge the two arrows
        for (int i = 0; i < child.objectsToPointAt.Count; i++) {
            parent.AddObjectToPointAt(child.objectsToPointAt[i].gameObject);
            snailToArrow.Remove(child.objectsToPointAt[i].gameObject);

            snailToArrow.Add(child.objectsToPointAt[i].gameObject, parent);
        }

        arrowsAroundPlayer.Remove(child);
        Destroy(child.gameObject);
        parent.PointAt();
    }

    void CreateNewArrow(GameObject objectToPointAt) {
        GameObject newArrow = (GameObject)Instantiate(arrowPrefab, this.transform.position, Quaternion.identity);
        newArrow.transform.parent = this.transform;
        newArrow.GetComponent<ArrowPointer>().arrowMan = this;
        newArrow.GetComponent<ArrowPointer>().AddObjectToPointAt(objectToPointAt);
        arrowsAroundPlayer.Add(newArrow.GetComponent<ArrowPointer>());

        if (snailToArrow.ContainsKey(objectToPointAt))
            snailToArrow.Remove(objectToPointAt);

        snailToArrow.Add(objectToPointAt, newArrow.GetComponent<ArrowPointer>());
        newArrow.GetComponent<ArrowPointer>().PointAt();
    }

    bool CheckIfCreateNewArrow(GameObject snail) {
        if (snail == null)
            return false;
        // First we need to check if the object already has an arrow
        if (snailToArrow.ContainsKey(snail)) {
            return false;
        }


        // Then we need to check if the angle difference is big enough 
        float tempAngle = CalculateAngle(snail);
        foreach (ArrowPointer arrow in arrowsAroundPlayer)
        {
            if (Mathf.Abs(Mathf.Abs(tempAngle) - Mathf.Abs(arrow.currentAngle)) < mergeAngle) {
                //arrow.AddObjectToPointAt(snail);
                snailToArrow.Add(snail, arrow);
                return false;
            }
        }

        // Else create a new one
        CreateNewArrow(snail);
        return true;
    }

    float CalculateAngle(GameObject refObject)
    {
        return Mathf.Atan((-refObject.transform.position.y)/(-refObject.transform.position.x))*(180/Mathf.PI);
    }
}
