﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GardenModeManager : MonoBehaviour {

    public static GardenModeManager instance;

    public Transform[] plantTransforms;
    public GardenModePlantArea[] plantAreas;

    [SerializeField] private GameObject seedPickerUI;
    [SerializeField] private Transform scrollWindowContent;
    [SerializeField] private GameObject mobileInputUI;
    [SerializeField] private GameObject noSeedsInInventoryText;

    [SerializeField] private GameObject plantAreaPrefab;
    [SerializeField] private GameObject seedWindow_Prefeb;

    private List<GameObject> seedWindows = new List<GameObject>();
    public int currentPlantPos;

    void Awake() {
        instance = this;
    }

    void Start() {
        plantAreas = new GardenModePlantArea[plantTransforms.Length];
        seedPickerUI.SetActive(false);
        LoadPlantAreas();
    }

    void LoadPlantAreas() {
        for (int i = 0; i < plantTransforms.Length; i++)
        {
            GameObject plantArea = (GameObject)Instantiate(plantAreaPrefab, plantTransforms[i].position, Quaternion.identity);
            plantArea.transform.parent = plantTransforms[i];
            plantAreas[i] = plantArea.GetComponent<GardenModePlantArea>();
            plantArea.GetComponent<GardenModePlantArea>().position = i;

            // Here we 'passar på' and see if we have a seed in this plant area
            if (Profile.current.plantAtPos.ContainsKey(i)) {
                LoadAlreadyPlantedSeed(Profile.current.plantAtPos[i].seedType, i, Profile.current.plantAtPos[i].plantTime);
            }
        }
    }

    void SaveProfileGardenModeData(SeedData.SeedType type, int pos) {
        // Save in a dictionary? With key = position and value is ISeed?
        if (!Profile.current.plantAtPos.ContainsKey(pos)) {
            Profile.current.StoreInfoAboutPlant(type, pos);
        }
    }

    void LoadSeedPicker()
    {
        foreach (KeyValuePair<SeedData.SeedType, int> seed in Profile.current.ownedSeeds)
        {
            GameObject wind = (GameObject)Instantiate(seedWindow_Prefeb, scrollWindowContent.position, Quaternion.identity);
            wind.transform.SetParent(scrollWindowContent);

            if (seedWindows.Count == 0)
                wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(215f, -25f);
            else
                wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(215f + (seedWindows.Count * 215f), -25f); ;

            wind.GetComponent<RectTransform>().localScale = Vector3.one;

            wind.GetComponent<SeedWindow>().DisplaySeed(ItemsManager.instance.seedType_to_GO[seed.Key].GetComponent<ISeed>());
            seedWindows.Add(wind);
        }

        if (Profile.current.ownedSeeds.Count <= 0)
        {
            noSeedsInInventoryText.SetActive(true);
        }
        else {
            noSeedsInInventoryText.SetActive(false);
        }

        scrollWindowContent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2((seedWindows.Count * 215f) + 50f, 0f);
    }

    public void PlantSeedInPlantArea(SeedData.SeedType type) {

        Profile.current.ownedSeeds[type]--;
        if (Profile.current.ownedSeeds[type] <= 0) {
            Profile.current.ownedSeeds.Remove(type);
        }

        ISeed newSeed = ItemsManager.instance.seedType_to_GO[type].GetComponent<ISeed>();
        newSeed.PlantTime = DateTime.MinValue;
        plantAreas[currentPlantPos].DisplayPlant(newSeed);
        SaveProfileGardenModeData(type, currentPlantPos);
        HideSeedPicker();
    }

    public void LoadAlreadyPlantedSeed(SeedData.SeedType type, int pos, DateTime plantTime)
    {
        ISeed loadedSeed = ItemsManager.instance.seedType_to_GO[type].GetComponent<ISeed>();
        loadedSeed.PlantTime = plantTime;
        plantAreas[pos].DisplayPlant(loadedSeed);
    }

    public void DisplaySeedPicker(int plantPos) {
        currentPlantPos = plantPos;
        seedPickerUI.SetActive(true);
        mobileInputUI.SetActive(false);
        LoadSeedPicker();
    }

    public void HideSeedPicker() {
        currentPlantPos = new int();
        seedPickerUI.SetActive(false);
        mobileInputUI.SetActive(true);

        foreach (GameObject child in seedWindows) {
            Destroy(child);
        }

        seedWindows.Clear();
    }
}
