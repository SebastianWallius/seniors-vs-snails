﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class ProfileManager : MonoBehaviour {

    public static ProfileManager instance;

    // Saved games
    public static Profile[] profiles = new Profile[3]; // 3 save slots

    [SerializeField] private static int currentProfile = -1;


    void Awake() {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        LoadFromFile();

        if (SceneManager.GetActiveScene().name != "CompentusLogo" && SceneManager.GetActiveScene().name != "LoadingScreen" && SceneManager.GetActiveScene().name != "ProfileScreen" && currentProfile == -1)
        {
            Debug.Log("Selecting profile 1");
            if (!SelectProfile(0))
                CreateNewProfile("Admin");
        }

    }



    public bool SelectProfile(int nr) {
        try {
            if (profiles[nr] != null)
            { // If there is a profile here to load

                profiles[nr].SetCurrentProfile();
                currentProfile = nr;
                return true;
            }
            else { // If there's not a profile, set current profile and await a new one
                currentProfile = nr;
                return false;
            }

        } catch {
            Debug.LogError("Could not select profile! The index might be out of range!");
        }

        return false;

    }

    public void ResetProfile(int nr) {

        try {
            profiles[nr] = null;
            Profile.current = null;
            SaveProfile();
        } catch {
            Debug.LogError("Could not reset profile! The index might be out of range!");
        }
    }

    public void ResetCurrentProfile(){
        try {
            profiles[currentProfile].ResetProfile();
            profiles[currentProfile] = null;
            SaveProfile();
        } catch {
            Debug.LogError("Could not reset profile! The index might be out of range!");
        }
    }

    static void LoadFromFile() {
        if (File.Exists(Application.persistentDataPath + "/savedProfiles.gd")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedProfiles.gd", FileMode.Open);
            profiles = (Profile[])bf.Deserialize(file);
            file.Close();
        }
    }

    public void SaveProfile() {
        SaveToFile();
    }

    public Profile GetCurrentProfile() {
        if (profiles[currentProfile] != null) {
            return profiles[currentProfile];

        } else {
            Debug.LogError("Cannot load selected profile! Is it missing?");
            return null;
        }
    }

    public Profile GetProfileInfo(int number) {
        if (profiles[number] != null) {
            return profiles[number];

        } else {
            return null;
        }
    }

    public void CreateNewProfile(string name) {
        profiles[currentProfile] = new Profile(name);
        SaveProfile();
    }

    static void SaveToFile() {
        if(Profile.current != null)
            Debug.Log("Saving profile: " + Profile.current.profileName);

        profiles[currentProfile] = Profile.current;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedProfiles.gd");
        bf.Serialize(file, profiles);
        file.Close();
    }

    void OnApplicationQuit() {
        SaveProfile();
    }
}
