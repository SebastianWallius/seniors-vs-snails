﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    //private UIManager uiMan;
    //private SoundManager soundMan;


    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        //uiMan = GetComponent<UIManager>();
        //soundMan = GetComponent<SoundManager>();

        // Load all required assets:
    }

    void Start() {
        
    }

    public string GetCurrentScene() {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadLevel(int level) {
        // Here we load the selected level
        // Called from the UIManager
        UnPauseGame();
        SceneManager.LoadScene("Area_" + level.ToString());
        SoundManager.instance.PlayMusic(SoundManager.instance.world1_Bg);
    }

    public void RestartScene() {
        UnPauseGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ChangeScene(string scene) {
        // Always unpause the game when loading a new scene
        UnPauseGame();
        SceneManager.LoadScene(scene);

        if (scene == "ShopMode" || scene == "InventoryMode")
        {
            SoundManager.instance.PlayMusic(SoundManager.instance.shop_elevatorMusic);
        } else if ((scene == "LoadingScreen" || scene == "MainMenu") && SoundManager.instance.audS_Music.clip != SoundManager.instance.mainMenuMusic) {
            SoundManager.instance.PlayMusic(SoundManager.instance.mainMenuMusic);

        } else if (scene != "MainMenu" && SoundManager.instance.audS_Music.clip != SoundManager.instance.world1_Bg) {
            SoundManager.instance.PlayMusic(SoundManager.instance.world1_Bg);
        }
    }

    public void ToggleVibrations(bool on) {
        Vibration.isEnabled = on;
    }

    public void PauseGame() {
        Time.timeScale = 0f;
    }

    public void UnPauseGame() {
        Time.timeScale = 1f;
    }
}
