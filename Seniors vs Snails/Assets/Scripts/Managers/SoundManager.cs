﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance = null;
    public AudioSource audS_Music;
    public AudioSource audS_Fx;

    public float musicVol = 1f;
    public float fxVol = 1f;

    private bool isPlayingMusic = false;

    public AudioClip world1_Bg;
    public AudioClip shop_elevatorMusic;
    public AudioClip mainMenuMusic;

    // Sound clip Data:
    // SENIOR:
    public AudioClip[] senior_Walk;
    public AudioClip[] senior_Attack_Miss;
    public AudioClip[] senior_Attack_Hit;

    // SNAIL:
    public AudioClip[] snail_Eat;
    public AudioClip[] snail_EatCrispy;
    public AudioClip[] snail_Die;

    // SEED:
    public AudioClip[] seed_PickUp;

    // CASH:
    public AudioClip[] cash_PickUp;

   

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != null) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Update() {
        if (isPlayingMusic == true) {
            audS_Music.volume = Mathf.Lerp(audS_Music.volume, musicVol, 0.01f);
        }
    }

    public void PlaySoundFx(AudioClip[] typeOfSound) {
        int randSound = Random.Range(0, typeOfSound.Length);
        audS_Fx.PlayOneShot(typeOfSound[randSound], 1f);
    }


    public void PlayMusic(AudioClip music) {
        musicVol = 1f;
        audS_Music.volume = 0f;
        audS_Music.clip = music;
        audS_Music.time = Random.Range(0f, music.length);
        audS_Music.Play();

        isPlayingMusic = true;
    }

    public void SetMusicVolume(float vol) {
        audS_Music.volume = vol;
    }

    public void SetFxSoundVolume(float vol) {
        audS_Fx.volume = vol;
    }


}
