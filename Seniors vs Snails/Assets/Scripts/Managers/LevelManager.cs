﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LevelManager : MonoBehaviour {

    public static LevelManager instance;

    // This script handles the level
    public int areaNumber = 0;
    public int areaMoneyReward = 0;

    // Level attributes
    public bool spawningSnailsOnStart = false;
    public bool unlimitedSnails = false;

    // Dynamic variables
    private int totalSnailsCount = 0;
    private int spawnedSnailsCount = 0;
    private float currentTimer;

    // For keeping track of win condition
    private List<ISnail> listSnails = new List<ISnail>();
    private List<IFlower> listFlowers = new List<IFlower>();
    private GameObject[] flowers;
    private GameObject[] s_area;

    // All the snail blocks in the level
    [SerializeField] public List<EnemyBlock> levelSnailBlocks;
    private EnemyBlock currentBlock = null;


    private GameObject player;
    private bool levelCompleted = false;

    [HideInInspector] public bool currentlySpawningSnails = false;

    private GameObject winScreen;
    private GameObject loseScreen;
    private GameObject mobileInputObject;
    private GameObject menuWindowUIObject;

    // Use this for initialization
    void Awake() {
        instance = this;
        // Find all required gameObjects
        winScreen = GameObject.FindGameObjectWithTag("WinScreen");
        loseScreen = GameObject.FindGameObjectWithTag("LoseScreen");
        mobileInputObject = GameObject.Find("MobileSingleStickControl");
        menuWindowUIObject = GameObject.Find("MenuWindowUI");
        player = GameObject.FindGameObjectWithTag("Player");
        s_area = GameObject.FindGameObjectsWithTag("BgMat_SnailSpawnDirt");

        if (winScreen == null)
            Debug.LogError("Could not find any gameObject named WinScreen in the current scene!");
        if (loseScreen == null)
            Debug.LogError("Could not find any gameObject named LoseScreen in the current scene!");
        if (mobileInputObject == null)
            Debug.LogError("Could not find any gameObject named MobileSingleStickControl in the current scene!");
        if (menuWindowUIObject == null)
            Debug.LogError("Could not find any gameObject named menuWindowUIObject in the current scene!");
        if (player == null)
            Debug.LogError("Could not find any gameObject with tag Player in the current scene!");
        
        // Disable UI screens
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
    }

    void Start() {
        FindFlowersInScene();
        FindSnailsInScene();

        // Disable UI screens
        if (GameManager.instance.GetCurrentScene() != "GardenMode")
            mobileInputObject.SetActive(false);


        // If we should start spawning directly
        if (GetComponent<TutorialLevel>().tutorialInfo == TutorialManager.TutorialInfo.None)
        {
            spawningSnailsOnStart = true;
        }
        else
        {
            spawningSnailsOnStart = false;
        }

        if (spawningSnailsOnStart)
        {
            currentlySpawningSnails = true;
            mobileInputObject.SetActive(true);
        }

        // Count all snails for the win condition
        CountSnails();

        // Set up ignore layer collisions
        Physics.IgnoreLayerCollision(12, 11, true); // Player and snail ignore
        Physics.IgnoreLayerCollision(11, 12, true);

        Physics.IgnoreLayerCollision(12, 14, true); // Snail and map
        Physics.IgnoreLayerCollision(14, 12, true);

        Physics.IgnoreLayerCollision(11, 14, true); // Player and map
        Physics.IgnoreLayerCollision(14, 11, true);

        // Music
        if(SoundManager.instance.audS_Music.clip == null || SoundManager.instance.audS_Music.clip != SoundManager.instance.world1_Bg)
            SoundManager.instance.PlayMusic(SoundManager.instance.world1_Bg);

        StartCoroutine(WaitALittleTest());
    }

    IEnumerator WaitALittleTest() {
        yield return new WaitForSeconds(1f);
        Debug.Log(LevelCreation.ConvertCurrentSceneToLevelInfo());
    }



    // Finds all flowers in the scene
    void FindFlowersInScene() {
        flowers = GameObject.FindGameObjectsWithTag("Flower");

        foreach (GameObject f in flowers)
        {
            listFlowers.Add(f.GetComponent<IFlower>());
            AddFlowerToStatsRow(f);
        }
    }

    // Finds all snails in the scene (if any, there shouldn't be!)
    void FindSnailsInScene() {
        GameObject[] snailsInScene = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject s in snailsInScene)
        {
            listSnails.Add(s.GetComponent<ISnail>());
            AddSnailToStatsRow(s);
            totalSnailsCount++;
            spawnedSnailsCount++;
        }
    }

    // Here we count all the snails in the level, so that we know when the player has won
    private void CountSnails()
    {
        foreach (EnemyBlock block in levelSnailBlocks) {
            if (block == null) {
                levelSnailBlocks.Remove(block);
                break;
            }

            int tempSnailCount = 0;
            foreach (EnemyBlock.EnemyInstance snail in block.blockSnails) {
                totalSnailsCount += snail.count;
                tempSnailCount += snail.count;
            }
            block.totalSnailsInBlock = tempSnailCount;
        }
    }

    void CalculateWinScreen()
    {
        winScreen.GetComponent<WinLoseScreen>().LoadWinScreenUI(Profile.current.currentXP, Profile.current.currentXPlevel);
    }

    // Update is called once per frame
    void Update () {
        // SpawnTimer
        if (currentlySpawningSnails) {
            // If no block is selected, choose the first
            if (currentBlock == null && levelSnailBlocks.Count >= 1) {
                currentBlock = levelSnailBlocks[0];
                currentTimer = currentBlock.spawnInterval;
            }

            currentTimer -= Time.deltaTime;

            // Spawn
            if (currentTimer < 0 && spawnedSnailsCount < totalSnailsCount) {
                CalculateSnailSpawn();
                currentTimer = currentBlock.spawnInterval;
            }
        }


        // If win
        if (listSnails.Count <= 0 && spawnedSnailsCount == totalSnailsCount && !levelCompleted)
        {
            levelCompleted = true;

            if (Profile.current != null) {
                if (Profile.current.currentWorldLevel == areaNumber)
                {
                    //Profile.current.money += levelMoneyReward;
                    Profile.current.currentWorldLevel++;
                }
            }

            StartCoroutine(WaitShowWinLoseScreen(true));
        }// If lose
        else if (listFlowers.Count <= 0 && !levelCompleted) {
            // The level is "Completed" but not done, we need the show the player the losescreen
            levelCompleted = true;
            StartCoroutine(WaitShowWinLoseScreen(false));
        }
    }

    // A delay to see the win/lose screen
    IEnumerator WaitShowWinLoseScreen(bool win) {
        
        yield return new WaitForSeconds(3f);
        ProfileManager.instance.SaveProfile();

        if (win) {
            CalculateWinScreen();
            foreach (IFlower f in listFlowers) {
                RewardManager.instance.StoreMoney(f.SurviveReward);
            }
            winScreen.SetActive(true);
            RewardManager.instance.GiveStoredRewardToPlayer();
        }
        else
            loseScreen.SetActive(true);

        mobileInputObject.SetActive(false);
        menuWindowUIObject.SetActive(false);
    }

    private void CalculateSnailSpawn() {
        // Here we spawn all the snails!
        // The spawning is done in blocks of enemies

        // First, lets check if the current block has any snails left. If not, then we chose the next one
        if (totalSnailsCount >= 1 && currentBlock.totalSnailsInBlock <= 0 && levelSnailBlocks.Count > 1)
            NextBlock();

        // If the function SpawnSnail has been called, but we don't have any snails left to spawn, return.
        if (totalSnailsCount <= 0)
            return;
        

        // We choose the area to spawn
        int chosenArea;

        if (s_area.Length >= 1) {
            chosenArea = Random.Range(0, s_area.Length);
        } else { chosenArea = 0; }

        float xPos = Random.Range(s_area[chosenArea].transform.position.x - (s_area[chosenArea].transform.localScale.x / 2),
             s_area[chosenArea].transform.position.x + (s_area[chosenArea].transform.localScale.x / 2));
        float yPos = Random.Range(s_area[chosenArea].transform.position.y - (s_area[chosenArea].transform.localScale.y / 2),
            s_area[chosenArea].transform.position.y + (s_area[chosenArea].transform.localScale.y / 2));


        // Chose the snail
        EnemyBlock.EnemyInstance chosenSnail = null;

        // If Ordered Spawn
        if (currentBlock.blockType == EnemyBlock.BlockType.Ordered) {

            // We find next "spawnable" snail in the order
            foreach (EnemyBlock.EnemyInstance snail in currentBlock.blockSnails) {
                if (snail.count >= 1) {
                    chosenSnail = snail;
                    break;
                } else { currentBlock.blockSnails.Remove(snail); }
            }

        } else if (currentBlock.blockType == EnemyBlock.BlockType.Random) {
            while (currentBlock.totalSnailsInBlock > 0) {
                int randInt = Random.Range(0, currentBlock.blockSnails.Count);
                if (currentBlock.blockSnails[randInt].count > 0) {
                    chosenSnail = currentBlock.blockSnails[randInt];
                    break;
                } else { currentBlock.blockSnails.Remove(currentBlock.blockSnails[randInt]); }
            }

        } else if (currentBlock.blockType == EnemyBlock.BlockType.Wave) {
            foreach (EnemyBlock.EnemyInstance snail in currentBlock.blockSnails)
            {
                if (snail.count >= 1)
                {
                    chosenSnail = snail;
                    break;
                }
                else { currentBlock.blockSnails.Remove(snail); }
            }

            currentBlock.spawnInterval = 0;
        }



        SpawnSnail(chosenSnail, new Vector2(xPos, yPos));
    }
    // Here we spawn the chosen snail at the chosen position
    void SpawnSnail(EnemyBlock.EnemyInstance chosenSnail, Vector2 spawnPos) {
        if (totalSnailsCount >= 1 || unlimitedSnails)
        {
            GameObject snailEnemy = (GameObject)Instantiate(chosenSnail.prefab, spawnPos, Quaternion.identity);
            listSnails.Add(snailEnemy.GetComponent<ISnail>());
            AddSnailToStatsRow(snailEnemy);
            spawnedSnailsCount++;
            currentBlock.totalSnailsInBlock--;
            chosenSnail.count--;
        }
    }

    void NextBlock() {
        levelSnailBlocks.Remove(currentBlock);
        currentBlock = levelSnailBlocks[0];
    }


    public IFlower GetClosestFlower(Vector3 currPos) {

        // Temp values
        float dist = -1f;
        IFlower returnFlower = null;

        foreach (GameObject f in flowers)
        {
            if (Vector3.Distance(f.transform.position, currPos) < dist || dist == -1) {
                if (f.GetComponent<IFlower>().IsAlive) {
                    dist = Vector3.Distance(f.transform.position, currPos);
                    returnFlower = f.GetComponent<IFlower>();
                }
            }
        }
        return returnFlower;
    }

    public Vector3 GetPlayerPosition() {
        return player.transform.position;
    }

    public GameObject GetPlayer() {
        return player;
    }

    public void AddSnailToList(GameObject snail) {
        listSnails.Add(snail.GetComponent<ISnail>());
    }

    public void RemoveSnailFromList(GameObject snail) {
        listSnails.Remove(snail.GetComponent<ISnail>());
        GetComponent<RewardManager>().StoreXP(snail.GetComponent<ISnail>().XpGain);
    }

    public void RemoveFlowerFromList(IFlower flower) {
        listFlowers.Remove(flower);
        RemoveFlowerFromStatsRow(flower.FlowerGameObject);
    }

    public void AddFlowerToStatsRow(GameObject flower) {
        winScreen.GetComponent<WinLoseScreen>().AddFlowerToDisplayList(flower);
    }

    public void RemoveFlowerFromStatsRow(GameObject flower) {
        winScreen.GetComponent<WinLoseScreen>().RemoveFlowerFromDisplayList(flower);
    }

    public void AddSnailToStatsRow(GameObject snail) {
        winScreen.GetComponent<WinLoseScreen>().AddSnailToDisplayList(snail);
    }

    public void AddSeedToStatsRow(SeedData.SeedType seed) {
        winScreen.GetComponent<WinLoseScreen>().AddSeedToDisplayList(seed);
    }

    public void AddMoneyToStatsRow(GameObject money) {
        winScreen.GetComponent<WinLoseScreen>().AddMoneyToDisplayList(money);
    }

    public int FlowersStillInScene() {
        return listFlowers.Count;
    }

    public List<ISnail> GetLevelSnailList() {
        return listSnails;
    }

    public FlowersData.FlowerType[] GetLevelFlowerType() {
        FlowersData.FlowerType[] flowerTypes = new FlowersData.FlowerType[listFlowers.Count];
        int i = 0;
        foreach (IFlower flower in listFlowers) {
            flowerTypes[i] = flower.TypeOfFlower;
            i++;
        }
        return flowerTypes;
    }

    public Transform[] GetLevelFlowerPos()
    {
        Transform[] flowerPos = new Transform[listFlowers.Count];
        int i = 0;
        foreach (IFlower flower in listFlowers)
        {
            flowerPos[i] = flower.FlowerGameObject.transform;
            i++;
        }
        return flowerPos;
    }

    public void EnableSnailSpawning() {
        currentlySpawningSnails = true;
        mobileInputObject.SetActive(true);
    }

    public EnemyBlock[] GetLevelEnemyBlocks() {
        return levelSnailBlocks.ToArray();
    }

    public void LoadNextLevel() {
        GameManager.instance.LoadLevel(areaNumber + 1);
    }
}
