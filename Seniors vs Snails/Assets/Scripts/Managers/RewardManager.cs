﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(LevelManager))]
public class RewardManager : MonoBehaviour {

    // This class handles the money spawning when a snail has been killed
    public static RewardManager instance;

    [SerializeField] private GameObject moneyPrefab;


    // Stored values to be added at the end of the level
    [HideInInspector] private int levelStoredXp;
    [HideInInspector] private int levelStoredMoney;


    void Start() {
        instance = this;
    }

    public void StoreXP(int amt) {
        levelStoredXp += amt;
        SpecialAttackManager.instance.AddChargeToSpecialAttack(amt);
    }

    public void StoreMoney(int amt) {
        levelStoredMoney += amt;
    }

    public int GetStoredMoney() {
        return levelStoredMoney;
    }

    public int GetStoredXP() {
        return levelStoredXp;
    }

    public void GiveStoredRewardToPlayer() {
        Profile.current.money += levelStoredMoney;
        Profile.current.AddXP(levelStoredXp);
        levelStoredMoney = 0;
        levelStoredXp = 0;
    }


    public void SpawnMoneyFromTarget(int percentage, float maxMoney, float minMoney, Vector3 pos) {
        float chance = Random.Range(0f, 100f);

        if (chance <= percentage) {
            float randInt = Random.Range(minMoney, maxMoney);

            for (int i = 0; i < randInt; i++)
            {
                GameObject money_GO = (GameObject)Instantiate(moneyPrefab, pos, Quaternion.identity);
                StartCoroutine(ThrowItemInRandomDirection(money_GO, Random.Range(0.4f, 0.6f)));

            }
        }
    }

    public void SpawnSeedFromTarget(int percentage, SeedData.SeedType typeOfSeed, Vector3 pos) {
        float chance = Random.Range(0f, 100f);

        if (chance <= percentage) {
            GameObject seed_GO = (GameObject)Instantiate(ItemsManager.instance.seedType_to_GO[typeOfSeed], pos, Quaternion.identity);
            seed_GO.GetComponent<ISeed>().IsDropPrefab = true;
            StartCoroutine(ThrowItemInRandomDirection(seed_GO, Random.Range(0.4f, 0.6f)));
        }
    }

    IEnumerator ThrowItemInRandomDirection(GameObject item, float duration) {
        Vector3 targetPos = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f);
        targetPos = (targetPos * 3) + item.transform.position;
        item.GetComponent<Rigidbody2D>().gravityScale = 3f;
        Vector3 velo = ItemThrowCalculate.CalculateThrowArc(item.transform.position, targetPos, duration, item.GetComponent<Rigidbody2D>().gravityScale);
        item.GetComponent<Rigidbody2D>().AddForce(velo, ForceMode2D.Impulse);

        yield return new WaitForSeconds(duration);
        item.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        item.GetComponent<Rigidbody2D>().gravityScale = 0f;
    }    

}
