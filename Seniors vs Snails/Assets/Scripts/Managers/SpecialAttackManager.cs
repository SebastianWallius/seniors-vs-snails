﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;


public class SpecialAttackManager : MonoBehaviour {

    public static SpecialAttackManager instance;

    [HideInInspector] private ISpecialAttack specialAttackRef;


    [SerializeField] private Image specialAttackChargeFill;
    [SerializeField] private GameObject specialAttackButton;
    [SerializeField] private Animator buttonAnim;
    private bool canBeActivated;
    private bool updateFillAmount = false;

    void Start() {
        instance = this;

        if (Profile.current.equippedSpecialAttack != SpecialAttacksData.SpecialAttackType.None) {
            StartCoroutine(WaitForLoadPLayerSpecialAttack());
            specialAttackButton.SetActive(true);
        }
        else {
            specialAttackButton.SetActive(false);
        }
    }

    IEnumerator WaitForLoadPLayerSpecialAttack() {
        yield return new WaitForEndOfFrame();
        specialAttackRef = LevelManager.instance.GetPlayer().GetComponent<Player>().GetEquippedSpecialAttack().GetComponent<ISpecialAttack>();
        updateFillAmount = true;
        specialAttackRef.CurrentCharge = 0;
    }


    void Update() {
        

        if(updateFillAmount){
            float float1 = specialAttackRef.CurrentCharge;
            float float2 = specialAttackRef.ChargeRequirement;


            specialAttackChargeFill.fillAmount = Mathf.Lerp(specialAttackChargeFill.fillAmount, 
                (float1 / float2), 1f);
        }

        // If input
        if (canBeActivated && CrossPlatformInputManager.GetButton("m_SpecialAttack"))
        {
            ActivateSpecialAttack();
        }

    }

    public void AddChargeToSpecialAttack(int amt) {
        if (Profile.current.equippedSpecialAttack != SpecialAttacksData.SpecialAttackType.None) {
            specialAttackRef.CurrentCharge = Mathf.Clamp(specialAttackRef.CurrentCharge + amt, 0, specialAttackRef.ChargeRequirement);

            if (specialAttackRef.CurrentCharge >= specialAttackRef.ChargeRequirement)
            {
                canBeActivated = true;
                EnableSpecialAttackButton();
            }
        }
    }

    void EnableSpecialAttackButton() {
        buttonAnim.SetBool("SpecialAttackButtonIsVisible", true);
    }

    public void ActivateSpecialAttack() {
        specialAttackRef.ActivateSpecialAttack();
        specialAttackRef.CurrentCharge = 0;
        canBeActivated = false;
        DisableSpecialAttackButton();
    }

    public void DisableSpecialAttackButton() {
        buttonAnim.SetBool("SpecialAttackButtonIsVisible", false);
    }
}
