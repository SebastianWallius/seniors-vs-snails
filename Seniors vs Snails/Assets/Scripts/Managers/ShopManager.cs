﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopManager : MonoBehaviour {

    // This script manages all the buy-able items in the shop
    // Input: array of items, and the script sorts by cost, and displays in a prefabUI

    // External UI windows prefabs
    public GameObject weaponWindow_Prefab;
    public GameObject specialAttackWindow_Prefab;
    public GameObject outfitWindow_Prefab;

    // Internal references for keeping the window count in check
    [HideInInspector] public List<GameObject> weaponWindows = new List<GameObject>();
    [HideInInspector] public List<GameObject> specialAttackWindows = new List<GameObject>();
    [HideInInspector] public List<GameObject> outfitWindows = new List<GameObject>();

    // The scrollable window (where we instansiate our windows)
    public Transform scrollWindowContent;
    public bool isInventory;

    void Start() {
        LoadWeaponWindows();
    }

    void LoadWeaponWindows() {
        foreach (GameObject w_GO in ItemsManager.instance.weapons) {
            if (!isInventory && Profile.current.ownedWeapons.Contains(w_GO.GetComponent<IWeapon>().Weapon))
            {
                // Make owned weapons not appear
            }
            else if (isInventory && !Profile.current.ownedWeapons.Contains(w_GO.GetComponent<IWeapon>().Weapon))
            {
                // Make not owned weapons not appear
            }
            else {
                GameObject wind = (GameObject)Instantiate(weaponWindow_Prefab, scrollWindowContent.position, Quaternion.identity);
                wind.transform.SetParent(scrollWindowContent);

                if (weaponWindows.Count == 0)
                    wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f, 0f);
                else
                    wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f + (weaponWindows.Count * 450f), 0f); ;

                wind.GetComponent<RectTransform>().localScale = Vector3.one;

                wind.GetComponent<WeaponWindow>().shopMan = this;
                wind.GetComponent<WeaponWindow>().DisplayWeapon(w_GO.GetComponent<IWeapon>());
                weaponWindows.Add(wind);
            }
        }

        scrollWindowContent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2((weaponWindows.Count * 450f) + 50f, 0f);
    }

    void LoadSpecialAttackWindows() {
        foreach (GameObject sa_GO in ItemsManager.instance.specialAttack) {
            if (!isInventory && Profile.current.ownedSpecialAttacks.Contains(sa_GO.GetComponent<ISpecialAttack>().TypeOfSpecialAttack))
            {
                // Make owned special attacks not appear
            }
            else if (isInventory && !Profile.current.ownedSpecialAttacks.Contains(sa_GO.GetComponent<ISpecialAttack>().TypeOfSpecialAttack))
            {
                // Make not owned special attacks not appear
            }
            else
            {
                GameObject wind = (GameObject)Instantiate(specialAttackWindow_Prefab, scrollWindowContent.position, Quaternion.identity);
                wind.transform.SetParent(scrollWindowContent);

                if (specialAttackWindows.Count == 0)
                    wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f, 0f);
                else
                    wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f + (specialAttackWindows.Count * 450f), 0f); ;

                wind.GetComponent<RectTransform>().localScale = Vector3.one;

                wind.GetComponent<SpecialAttackWindow>().shopMan = this;
                wind.GetComponent<SpecialAttackWindow>().DisplaySpecialAttack(sa_GO.GetComponent<ISpecialAttack>());
                weaponWindows.Add(wind);
            }
        }

        scrollWindowContent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2((specialAttackWindows.Count * 450f) + 50f, 0f);
    }

    void LoadOutfitsWindows() {
        foreach (GameObject out_GO in ItemsManager.instance.outfits) {
            GameObject wind = (GameObject)Instantiate(outfitWindow_Prefab, scrollWindowContent.position, Quaternion.identity);
            wind.transform.SetParent(scrollWindowContent);

            if (outfitWindows.Count == 0)
                wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f, 0f);
            else
                wind.gameObject.GetComponent<RectTransform>().localPosition = new Vector2(250f + (outfitWindows.Count * 450f), 0f); ;

            wind.GetComponent<RectTransform>().localScale = Vector3.one;

            wind.GetComponent<OutfitWindow>().shopMan = this;
            wind.GetComponent<OutfitWindow>().DisplayOutfit(out_GO.GetComponent<Outfit>());
            outfitWindows.Add(wind);
        }

        scrollWindowContent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2((outfitWindows.Count * 450f) + 50f, 0f);
    }

    public void SortWindows(GameObject[] windowsToSort) {
        foreach (GameObject window in windowsToSort) {
            // Here we sort the display order

        }
    }

    public void DestroyCurrentWindows() {
        specialAttackWindows.Clear();
        weaponWindows.Clear();
        outfitWindows.Clear();
        foreach (Transform child in scrollWindowContent) {
            Destroy(child.gameObject);
        }
    }

    public void SelectWeaponsUI() {
        DestroyCurrentWindows();
        LoadWeaponWindows();
    }

    public void SelectSpecialAttacksUI() {
        DestroyCurrentWindows();
        LoadSpecialAttackWindows();
    }

    public void SelectOutfitsUI() {
        DestroyCurrentWindows();
        LoadOutfitsWindows();
    }

    public bool BuyWeapon(IWeapon w) {

        if (!Profile.current.ownedWeapons.Contains(w.Weapon))
        {
            if (Profile.current.money >= w.Cost)
            {
                Profile.current.money -= w.Cost;
                Profile.current.ownedWeapons.Add(w.Weapon);
                Profile.current.equippedWeapon = w.Weapon;
                return true;
            }
        }

        return false;
    }

    public bool BuySpecialAttack(ISpecialAttack specialAttack)
    {

        if (!Profile.current.ownedSpecialAttacks.Contains(specialAttack.TypeOfSpecialAttack))
        {
            if (Profile.current.money >= specialAttack.Cost)
            {
                Profile.current.money -= specialAttack.Cost;
                Profile.current.ownedSpecialAttacks.Add(specialAttack.TypeOfSpecialAttack);
                Profile.current.equippedSpecialAttack = specialAttack.TypeOfSpecialAttack;
                return true;
            }
        }

        return false;
    }

    public bool BuyOutfit(Outfit outfit)
    {

        if (!Profile.current.ownedOutfits.Contains(outfit.ID))
        {
            if (Profile.current.money >= outfit.Cost)
            {
                Profile.current.money -= outfit.Cost;
                Profile.current.ownedOutfits.Add(outfit.ID);


                Profile.current.EquipOutfit(outfit);
                return true;
            }
        }

        return false;
    }

}
