﻿using System.Collections.Generic;

public static class TileManager {

    // This class stores information about background materials and tags. Also handles the material speeds

    public enum TileType
    {
        Grass,
        Dirt,
        Gravel,
        SnailSpawnDirt,
        Water
    }

    public static string[] materialTags = {
        "BgMat_Grass",
        "BgMat_Dirt",
        "BgMat_Gravel",
        "BgMat_SnailSpawnDirt",
        "BgMat_Water"
    };

    public static string[] spriteNames = {
        "Grass_sprite",
        "Dirt_sprite",
        "Gravel_sprite",
        "SnailSpawnDirt_sprite",
        "Water_sprite"
    };

    public static Dictionary<TileType, string> tileTypeToSprite = new Dictionary<TileType, string>() {
        { TileType.Grass, spriteNames[0] },
        { TileType.Dirt, spriteNames[1] },
        { TileType.Gravel, spriteNames[2] },
        { TileType.SnailSpawnDirt, spriteNames[3] },
        { TileType.Water, spriteNames[4] }
    };

    public static Dictionary<TileType, string> tileTypeToMatTag = new Dictionary<TileType, string>() {
        { TileType.Grass, materialTags[0] },
        { TileType.Dirt, materialTags[1] },
        { TileType.Gravel, materialTags[2] },
        { TileType.SnailSpawnDirt, materialTags[3] },
        { TileType.Water, materialTags[4] }

    };

    public static Dictionary<string, TileType> matTagToTileType = new Dictionary<string, TileType>() {
        { materialTags[0], TileType.Grass },
        { materialTags[1], TileType.Dirt },
        { materialTags[2], TileType.Gravel },
        { materialTags[3], TileType.SnailSpawnDirt },
        { materialTags[4], TileType.Water }

    };

    public static float GetMaterialSpeedSenior(TileType mat)
    {
        switch (mat)
        {
            case TileType.Grass:
                return 1f;
            case TileType.Dirt:
                return 0.9f;
            case TileType.Gravel:
                return 1.3f;
            case TileType.SnailSpawnDirt:
                return 0.6f;
            case TileType.Water: // Can't be walked on anyway
                return 1f;
        }
        return 1;
    }

    public static float GetMaterialSpeedSnail(TileType mat)
    {
        switch (mat)
        {
            case TileType.Grass:
                return 1f;
            case TileType.Dirt:
                return 1.2f;
            case TileType.Gravel:
                return 0.7f;
            case TileType.SnailSpawnDirt:
                return 1.5f;
            case TileType.Water: // Can't be walked on anyway
                return 1f;
        }
        return 1;
    }
}
