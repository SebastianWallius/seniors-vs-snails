﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelInfo {

    // This class contains info about a level that we can store, maybe in a database?

    public LevelMapInfo mapInfo;
    public LevelObjectsInfo objectsInfo;
    public LevelPlayerInfo playerInfo;
    public LevelFlowersInfo flowersInfo;
    public LevelSnailsInfo snailsInfo;
    public string gameVersion; // A map is connected to a certain gameVersion, newer maps can't be played on older clients



    public LevelInfo(int mapSizeX, int mapSizeY, TileManager.TileType[] mapTileTypes,
        StaticObjectsData.StaticObjectType[] mapObjectsType, Transform[] mapObjectsPos,
        Transform playerPos, IWeapon playerEqWeapon, ISpecialAttack playerEqSpecialAttack,
        FlowersData.FlowerType[] mapFlowerTypes, Transform[] mapFlowerPos,
        EnemyBlock[] mapSnailBlocks) {

        mapInfo = new LevelMapInfo(mapSizeX, mapSizeY, mapTileTypes);
        objectsInfo = new LevelObjectsInfo(mapObjectsType, mapObjectsPos);
        playerInfo = new LevelPlayerInfo(playerPos, playerEqWeapon, playerEqSpecialAttack);
        flowersInfo = new LevelFlowersInfo(mapFlowerTypes, mapFlowerPos);
        snailsInfo = new LevelSnailsInfo(mapSnailBlocks);
    }


    public override string ToString() {
        string output = "Saving level... \n\n";

        // Map:
        output += "Map: \n";
        output += "Size: " + mapInfo.sizeX.ToString() + " X, " + mapInfo.sizeY.ToString() + " Y" + "\n";
        output += "Tiles: {";

        for (int i = 0; i < mapInfo.tileTypes.Length; i++){
            if (i == mapInfo.tileTypes.Length)
            {
                output += "Tile" + (i + 1) + "=" + mapInfo.tileTypes[i].ToString();
            } else {
                output += "Tile" + (i + 1) + "=" + mapInfo.tileTypes[i].ToString() + ", ";
            }
        }

        output += "}; \n\n";

        // Static object:
        output += "Static objects: \n";

        for (int i = 0; i < objectsInfo.objectTypes.Length; i++)
        {
            output += objectsInfo.objectTypes[i].ToString() + ", Pos: " + objectsInfo.objectPos[i].position.ToString() + ", Rot: " + objectsInfo.objectPos[i].rotation.ToString() + "\n";
        }

        output += "\n";

        // Player:
        output += "Player: \n";
        output += "Pos: " + playerInfo.playerPos.position.ToString() + ", Rot: " + playerInfo.playerPos.rotation.ToString() + "\n";
        output += "Equipped Weapon: " + playerInfo.eqWeapon.WeaponName + "\n";
        output += "Equipped Special Attack: " + playerInfo.eqSpecialAttack.Name + "\n";
        output += "\n";

        // Flowers:
        output += "Flowers: \n";

        for (int i = 0; i < flowersInfo.flowerTypes.Length; i++)
        {
            output += flowersInfo.flowerTypes[i].ToString() + ", Pos: " + flowersInfo.flowerPos[i].transform.ToString() + ", Rot: " + flowersInfo.flowerPos[i].rotation.ToString() + "\n";
        }

        output += "\n";

        // Snails:
        output += "Snails: \n";
        output += "Blocks: \n\n";

        for (int i = 0; i < snailsInfo.snailBlocks.Length; i++)
        {
            output += "\tBlock " + (i + 1) + ": \n";
            output += "\tType: " + snailsInfo.snailBlocks[i].blockType.ToString() + "\n";
            output += "\tInterval: " + snailsInfo.snailBlocks[i].spawnInterval.ToString() + " Seconds" + "\n";
            output += "\tCount: " + snailsInfo.snailBlocks[i].totalSnailsInBlock.ToString() + " snails" + "\n";
            output += "\tBlockSnails: \n";

            for (int p = 0; p < snailsInfo.snailBlocks[i].blockSnails.Count; p++)
            {
                output += "\t\tSnail: " + snailsInfo.snailBlocks[i].blockSnails[p].prefab.GetComponent<ISnail>().TypeOfSnail.ToString() + ", Count: " + snailsInfo.snailBlocks[i].blockSnails[p].count + " \n";
            }

        }

        output += "\n";

        return output;
    }



    [System.Serializable]
    public class LevelMapInfo {

        // This class contains the level map info. 

        public int sizeX;
        public int sizeY;
        public TileManager.TileType[] tileTypes;

        public LevelMapInfo(int x, int y, TileManager.TileType[] t) {
            sizeX = x;
            sizeY = y;
            tileTypes = t;
        }
    }

    [System.Serializable]
    public class LevelObjectsInfo {

        // This class contains the level static objects info.

        public StaticObjectsData.StaticObjectType[] objectTypes;
        public Transform[] objectPos;

        public LevelObjectsInfo(StaticObjectsData.StaticObjectType[] o,Transform[] t) {
            objectTypes = o;
            objectPos = t;
        }
    }

    [System.Serializable]
    public class LevelPlayerInfo {

        // This class contains the player info. (NOT TO BE CONFUSED WITH PROFILE)
        // Level authors can change the player variables when creating a level, but those are only temporary

        public Transform playerPos;
        public IWeapon eqWeapon;
        public ISpecialAttack eqSpecialAttack;

        public LevelPlayerInfo(Transform t, IWeapon w, ISpecialAttack s) {
            playerPos = t;
            eqWeapon = w;
            eqSpecialAttack = s;
        }
    }

    [System.Serializable]
    public class LevelFlowersInfo {

        // This class contains the level flower info.

        public FlowersData.FlowerType[] flowerTypes;
        public Transform[] flowerPos;

        public LevelFlowersInfo(FlowersData.FlowerType[] f, Transform[] t) {
            flowerTypes = f;
            flowerPos = t;
        }

    }

    [System.Serializable]
    public class LevelSnailsInfo {

        // This class contains the level snail block info.

        public EnemyBlock[] snailBlocks;

        public LevelSnailsInfo(EnemyBlock[] b) {
            snailBlocks = b;
        }

    }
}
