﻿using UnityEngine;
using System.Collections;

public class GameOverMenu : MonoBehaviour
{
    public AudioClip snailWin;

    public PlayMenu playMenu;
    Canvas gameOverMenu;

    void Awake()
    {
        gameOverMenu = GetComponent<Canvas>();
        SetGameMenuVisibility(false);
    }

    public void SetGameMenuVisibility(bool isVisible)
    {
        gameOverMenu.enabled = isVisible;
        SoundManager smInstance = SoundManager.instance;

        if (isVisible)
        {
            smInstance.PlaySingle(snailWin);
        }
        else
        {
            if (!smInstance.musicSource.isPlaying)
                smInstance.musicSource.Play();
        }
    }

    public void Retry()
    {
        SetGameMenuVisibility(false);
        GameManager.instance.SetActiveLevel(GameManager.instance.lastLevel);
        playMenu.ToggleLevelCanvasVisibility(true);
    }
}
