﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VibrationSettings : MonoBehaviour {

    public Toggle vibrationToggle;

    public void OnEnable()
    {
        vibrationToggle.isOn = GameManager.instance.vibrationActive;
    }

    public void OnValueChanged()
    {
        GameManager.instance.vibrationActive = vibrationToggle.isOn;
    }
	/*void Awake()
    {
        vibrationToggle.isOn = GameManager.instance.vibrationActive;
        Debug.Log("vibration is " + GameManager.instance.vibrationActive);
    }*/
}
