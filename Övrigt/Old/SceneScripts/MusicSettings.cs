﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MusicSettings : MonoBehaviour {

    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;

    public void SetMusicSourceVolume()
    {
        SoundManager.instance.musicSource.volume = musicVolumeSlider.value;
    }

    public void SetSfxSourceVolume()
    {
        SoundManager.instance.sfxSource.volume = sfxVolumeSlider.value;
    }
}
