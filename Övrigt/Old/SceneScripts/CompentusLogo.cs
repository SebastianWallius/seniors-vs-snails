﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CompentusLogo : MonoBehaviour {

    public FadeManager fadeManager;

    public int timeToShowLogo;
    public int waitTime;

    private Status status = Status.FadeIn;

    void Awake () {
    }

    void Update()
    {
		if (Application.isShowingSplashScreen)
			return;

        switch (status)
        {
            case Status.FadeIn:
                StartCoroutine(FadeIn());
                break;

            case Status.FadeOut:
                StartCoroutine(FadeOut());
                break;

            case Status.ChangeScene:
                StartCoroutine(ChangeScene());
                break;
        }
    }

    IEnumerator FadeIn()
    {
        StartCoroutine(fadeManager.FadeIn());
        while (fadeManager.isFadingIn)
        {
            yield return null;
        }
        yield return new WaitForSeconds(timeToShowLogo);
        status = Status.FadeOut;
    }

    IEnumerator FadeOut()
    {
        StartCoroutine(fadeManager.FadeOut());
        while (fadeManager.isFadingOut)
        {
            yield return null;
        }
        status = Status.ChangeScene;
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("LoadingScreen"); ;
    }
}

public enum Status
{
    FadeIn,
    FadeOut,
    ChangeScene
}