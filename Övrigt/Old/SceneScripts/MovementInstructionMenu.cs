﻿using UnityEngine;

public class MovementInstructionMenu : MonoBehaviour {

    private Canvas movementInstructionMenuCanvas;

    void Awake()
    {
        movementInstructionMenuCanvas = GetComponent<Canvas>();
        GameManager gmInstance = GameManager.instance;
        LevelInfo levelInfo = gmInstance.levelManager.GetLevelInfo(gmInstance.activeLevel);

        SetMovementInstructionMenuVisibility(gmInstance.activeLevel == 1 && !levelInfo.IsFinished);
    }

	public void SetMovementInstructionMenuVisibility(bool isVisible)
    {
        movementInstructionMenuCanvas.enabled = isVisible;
    }
}
