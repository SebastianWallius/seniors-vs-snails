﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class WorldLevels : MonoBehaviour {
    private Button[] buttons;
	void Awake () {
        buttons = GetComponentsInChildren<Button>();
        for(int i=0; i<buttons.Length-1; i+=1)
        {
            if (!buttons[i].name.Contains("Back"))
            {
                string levelNumber = Regex.Match(buttons[i].name, @"\d+").Value;
                if (string.IsNullOrEmpty(levelNumber))
                    break;
                buttons[i].interactable = int.Parse(levelNumber) <= GameManager.instance.playerLevel;
            }
        }
	}
}
