﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TopBar : MonoBehaviour {

    public Text moneyText;
    public Text livesText;
    public Text experienceText;

    void Start()
    {
        moneyText.text = GameManager.instance.playerMoney.ToString();
        livesText.text = GameManager.instance.playerLives.ToString();
        experienceText.text = GameManager.instance.playerExperience.ToString();
    }
}
