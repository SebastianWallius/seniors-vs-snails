﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {
    public FadeManager fadeManager;

    public int waitTime;
	void Start ()
    {
        SoundManager.instance.musicSource.Play();
        StartCoroutine(FadeIn());
    }

    IEnumerator FadeIn()
    {
        StartCoroutine(fadeManager.FadeIn());
        while(fadeManager.isFadingIn)
        {
            yield return null;
        }
        StartCoroutine(ChangeScene(waitTime));
    }
    IEnumerator ChangeScene(int waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        SceneManager.LoadScene("WorldView"); ;
    }
}
