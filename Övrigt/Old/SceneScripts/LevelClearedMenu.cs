﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LevelClearedMenu : MonoBehaviour {
    public PlayMenu playMenu;
    public Text levelClearedText;
    public Text experienceRewardText;
    public Text moneyRewardText;

    public AudioClip seniorWin;

    public GameObject moneyReward;
    public GameObject continueButton;

    Canvas levelClearedMenu;

    void Awake()
    {
        levelClearedMenu = GetComponent<Canvas>();
        ToggleLevelClearedCanvas(false);
    }

    public void ToggleLevelClearedCanvas(bool isVisible)
    {
        levelClearedMenu.enabled = isVisible;
        GameManager gmInstance = GameManager.instance;
        SoundManager smInstance = SoundManager.instance;

        if (isVisible)
        {
            smInstance.PlaySingle(seniorWin);

            levelClearedText.text = gmInstance.playerLevel <= 10 ? "Level " + gmInstance.lastLevel + " cleared!" : "World 1 finished!";

            LevelInfo levelInfo = gmInstance.levelManager.GetLevelInfo(gmInstance.lastLevel);
            int experienceReward = levelInfo.BoardInfo.NumberOfEnemies * 10 + levelInfo.BoardInfo.NumberOfSecondEnemies * 20 + levelInfo.BoardInfo.NumberOfBosses * 50;
            experienceRewardText.text = Convert.ToString(experienceReward);

            moneyRewardText.text = levelInfo.MoneyReward.ToString();
            continueButton.SetActive(gmInstance.playerLevel <= 10);
            moneyReward.SetActive(!levelInfo.HasPlayedLevel);
            levelInfo.HasPlayedLevel = true;
        }
    }

    public void ContinuePlaying()
    {
        ToggleLevelClearedCanvas(false);
        GameManager.instance.SetActiveLevel(GameManager.instance.lastLevel + 1);
        playMenu.ToggleLevelCanvasVisibility(true);
    }
}
