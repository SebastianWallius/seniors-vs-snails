﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UserPage : MonoBehaviour {

    public Text enemiesKilledText;
    public Text userDeathsText;

    void Start()
    {
        GameManager gmInstance = GameManager.instance;
        enemiesKilledText.text = gmInstance.numberOfEnemiesKilled.ToString();
        userDeathsText.text = gmInstance.numberOfDeaths.ToString();
    }
}
