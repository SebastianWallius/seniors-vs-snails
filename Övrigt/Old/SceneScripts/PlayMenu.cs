﻿using UnityEngine;
using UnityEngine.UI;

public class PlayMenu : MonoBehaviour {
    public Text canvasText;
    public Text snailText;
    public Text secondSnailText;
    public Text flowerText;
    public Text bossText;

    public GameObject snailImage;
    public GameObject secondSnailImage;
    public GameObject bossImage;
    public GameObject flowerImage;

    public GameObject rewardInfo;

    private Canvas playMenuCanvas;

    void Awake()
    {
        playMenuCanvas = GetComponent<Canvas>();
        ToggleLevelCanvasVisibility(false);
    }

    public void ToggleLevelCanvasVisibility(bool isVisible)
    {
        playMenuCanvas.enabled = isVisible;
        int level = GameManager.instance.GetActiveLevel();
        if (isVisible)
        {
            LevelInfo levelInfo = GameManager.instance.levelManager.GetLevelInfo(GameManager.instance.activeLevel);

            snailImage.SetActive(levelInfo.BoardInfo.NumberOfEnemies > 0);
            secondSnailImage.SetActive(levelInfo.BoardInfo.NumberOfSecondEnemies > 0);
            flowerImage.SetActive(levelInfo.BoardInfo.NumberOfFlowers > 0);
            bossImage.SetActive(levelInfo.BoardInfo.NumberOfBosses > 0);

            canvasText.text = "Level " + level;
            snailText.text = GameManager.instance.levelManager.GetLevelInfo(level).BoardInfo.NumberOfEnemies.ToString();
            secondSnailText.text = GameManager.instance.levelManager.GetLevelInfo(level).BoardInfo.NumberOfSecondEnemies.ToString();
            flowerText.text = GameManager.instance.levelManager.GetLevelInfo(level).BoardInfo.NumberOfFlowers.ToString();
            bossText.text = GameManager.instance.levelManager.GetLevelInfo(level).BoardInfo.NumberOfBosses.ToString();

            rewardInfo.SetActive(!levelInfo.IsFinished);
        }
    }
}
