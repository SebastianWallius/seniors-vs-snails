﻿public class PlayerInfo {

    public int Lives { get; set; }
    public int Points { get; set; }
    public int Experience { get; set; }

    public PlayerInfo (int points, int experience)
    {
        Points = points;
        Experience = experience;
    }
}
