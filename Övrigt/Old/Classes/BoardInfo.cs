﻿public class BoardInfo {

    public int Rows { get; set; }
    public int Columns { get; set; }
    public int NumberOfEnemies { get; set; }
    public int NumberOfSecondEnemies { get; set; }
    public int NumberOfBosses { get; set; }
    public int NumberOfFlowers { get; set; }

    public BoardInfo() { }

    public BoardInfo(int rows, int columns, int nrOfEnemies, int nrOfSecondEnemies, int nrOfBosses, int nrOfFlowers)
    {
        Rows = rows;
        Columns = columns;
        NumberOfEnemies = nrOfEnemies;
        NumberOfSecondEnemies = nrOfSecondEnemies;
        NumberOfBosses = nrOfBosses;
        NumberOfFlowers = nrOfFlowers;
    }

    public void UpdateBoardInfo(BoardInfo boardInfo)
    {
        Rows = boardInfo.Rows;
        Columns = boardInfo.Columns;
        NumberOfEnemies = boardInfo.NumberOfEnemies;
        NumberOfSecondEnemies = boardInfo.NumberOfSecondEnemies;
        NumberOfBosses = boardInfo.NumberOfBosses;
        NumberOfFlowers = boardInfo.NumberOfFlowers;
    }

    public void UpdateBoardInfo(int rows, int columns, int nrOfEnemies, int nrOfSecondEnemies, int nrOfBosses, int nrOfFlowers)
    {
        Rows = rows;
        Columns = columns;
        NumberOfEnemies = nrOfEnemies;
        NumberOfSecondEnemies = nrOfSecondEnemies;
        NumberOfBosses = nrOfBosses;
        NumberOfFlowers = nrOfFlowers;
    }
}
