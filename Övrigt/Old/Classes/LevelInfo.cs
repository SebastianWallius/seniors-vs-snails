﻿using System.Collections;

public class LevelInfo {

    public int Level { get; set; }

    public bool HasPlayedLevel { get; set; }

    public bool IsFinished { get; set; }

    public int LifeCost { get; set; }

    public int MoneyReward { get; set; }

    public BoardInfo BoardInfo { get; set; }

    public LevelInfo(BoardInfo boardInfo)
    {
        IsFinished = false;
        HasPlayedLevel = false;
        LifeCost = 1;
        MoneyReward = 1;
        BoardInfo = boardInfo;
    }

    public LevelInfo (int level, int rows, int columns, int nrOfEnemies, int nrOfSecondEnemies, int nrOfBosses, int nrOfFlowers)
    {
        Level = level;
        IsFinished = false;
        HasPlayedLevel = false;
        LifeCost = 1;
        MoneyReward = 1;
        BoardInfo = new BoardInfo(rows, columns, nrOfEnemies, nrOfSecondEnemies, nrOfBosses, nrOfFlowers);
    }
}
