﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

//Player inherits from MovingObject, our base class for objects that can move, Enemy also inherits from this.
public class Player : MovingObject
{
    public float restartLevelDelay = 1f;        //Delay time in seconds to restart level.
    public Vector3 startPosition;
    public AudioClip moveSound;

    public PlayerState playerState;

    //private Animator animator;                  //Used to store a reference to the Player's animator component.
    //private int points;                           //Used to store player food points total during level.

    //Start overrides the Start function of MovingObject
    protected override void Start()
    {
        //Call the Start function of the MovingObject base class.
        base.Start();
        playerState = PlayerState.Idle;
    }


    //This function is called when the behaviour becomes disabled or inactive.
    private void OnDisable()
    {
        //When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
        //GameManager.instance.playerPoints = points;
    }

    //AttemptMove overrides the AttemptMove function in the base class MovingObject
    //AttemptMove takes a generic parameter T which for Player will be of the type Wall, it also takes integers for x and y direction to move in.
    public override void AttemptMove<T>(int xDir, int yDir)
    {       
        //Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;

        if (playerState == PlayerState.Attacking)
        {
            /* Special case when having killed a snail
             * Snail is removed and is no longer blocking movement.
             * Delay Player movement on last attack. 
             */
            StartCoroutine(AttackDelay());
        }
        else
        {
            bool canMove = Move(xDir, yDir, out hit);
        
            if (canMove && GameManager.instance.CountEnemies() > 0)
            {
                //Register that Player is moving
                playerState = PlayerState.Moving;
                //Play movement sound
                SoundManager.instance.PlaySingle(moveSound);
            }
            else if (!canMove && playerState == PlayerState.Idle)
            {
                //Delay Player when attacking
                StartCoroutine(AttackDelay());
            }
        }
        
    }

    //OnCantMove overrides the abstract function OnCantMove in MovingObject.
    //It takes a generic parameter T which in the case of Player is a Wall which the player can attack and destroy.
    protected override void OnCantMove<T>(T component)
    {
        Enemy hitEnemy = component as Enemy;

        hitEnemy.LosePoint();

        if (GameManager.instance.vibrationActive)
            Handheld.Vibrate();

        CheckIfLevelCleared();
    }

    private void CheckIfLevelCleared()
    {
        if (GameManager.instance.CountEnemies() == 0)
        {
            GameManager.instance.LevelCleared();
        }
    }

    //Restart reloads the scene when called.
    private void Restart()
    {
        //Load the last scene loaded, in this case Main, the only scene in the game.
        SceneManager.LoadScene(SceneManager.GetActiveScene().ToString());
    }

    IEnumerator AttackDelay()
    {
        playerState = PlayerState.Attacking;
        yield return new WaitForSeconds(moveTime);
        playerState = PlayerState.Idle;
    }
}

public enum PlayerState
{
    Idle,
    Moving,
    Attacking
}
