﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//Enemy inherits from MovingObject, our base class for objects that can move, Player also inherits from this.
public class Enemy : MovingObject
{
    public Slider hitPointsSlider;

    public float hitPoints = 1f;
    public float totalHitPoints = 1f;
    public int attackPoints = 1;
    public int experiencePoints = 10;
    public bool isMoving = false;
    public AudioClip snailHit;
    public AudioClip snailDie;

    private Vector3 startPosition;
    //Start overrides the virtual Start function of the base class.
    protected override void Start()
    {
        //Register this enemy with our instance of GameManager by adding it to a list of Enemy objects. 
        //This allows the GameManager to issue movement commands.
        GameManager.instance.AddEnemyToList(this);

        //Call the start function of our base class MovingObject.
        base.Start();
    }

    void Update()
    {

    }

    //Override the AttemptMove function of MovingObject to include functionality needed for Enemy to skip turns.
    //See comments in MovingObject for more on how base AttemptMove function works.
    public override void AttemptMove<T>(int xDir, int yDir)
    {
        //Call the AttemptMove function from MovingObject.
        base.AttemptMove<T>(xDir, yDir);
    }

    IEnumerator Movement(int horizontal, int vertical)
    {
        isMoving = true;
        startPosition = transform.position;
        AttemptMove<Flower>(horizontal, vertical);
        while (this != null && transform.position != startPosition + new Vector3(horizontal, vertical, 0f))
        {
            yield return null;
        }
        isMoving = false;
    }

    //MoveEnemy is called by the GameManger each turn to tell each Enemy to try to move towards the player.
    public void MoveEnemy(Transform flower)
    {
        //Declare variables for X and Y axis move directions, these range from -1 to 1.
        //These values allow us to choose between the cardinal directions: up, down, left and right.
        int xDir = 0;
        int yDir = 0;

        int xDiff = (int) Mathf.Abs(flower.position.x - transform.position.x);
        int yDiff = (int) Mathf.Abs(flower.position.y - transform.position.y);

        //If the difference in the y dimension is larger than the difference in the x dimension:
        if (yDiff > xDiff)
        {
            //If the y coordinate of the target's (player) position is greater than the y coordinate of this enemy's position set y direction 1 (to move up). If not, set it to -1 (to move down).
            yDir = flower.position.y > transform.position.y ? 1 : -1;
        }

        //If the difference in the x dimension is larger than the difference in the y dimension:
        else if (xDiff > yDiff)
        {
            //Check if target x position is greater than enemy's x position, if so set x direction to 1 (move right), if not set to -1 (move left).
            xDir = flower.position.x > transform.position.x ? 1 : -1;
        }
        //If the difference in both dimensions are the same distance:
        else if (xDiff > 1)
        {
            xDir = flower.position.x > transform.position.x ? 1 : -1;
        }
        else if (xDiff == 1 && yDiff == 1)
        {
            yDir = flower.position.y > transform.position.y ? 1 : -1;
        }

        //Call the AttemptMove function and pass in the generic parameter Player, because Enemy is moving and expecting to potentially encounter a Player
        StartCoroutine(Movement(xDir, yDir));
    }


    //OnCantMove is called if Enemy attempts to move into a space occupied by a Player, it overrides the OnCantMove function of MovingObject 
    //and takes a generic parameter T which we use to pass in the component we expect to encounter, in this case Player
    protected override void OnCantMove<T>(T component)
    {
        isMoving = false;

        Flower hitFlower = component as Flower;

        hitFlower.LosePoint(attackPoints);

        if (GameManager.instance.vibrationActive)
            Handheld.Vibrate();

        CheckIfGameOver();
    }

    private void CheckIfGameOver()
    {
        if (GameManager.instance.CountFlowers() == 0)
        {
            GameManager.instance.GameOver();
        }
    }

    public void LosePoint()
    {
        hitPoints -= 1;

        GameManager gmInstance = GameManager.instance;
        SoundManager smInstance = SoundManager.instance;

        if (hitPoints > 0f)
            smInstance.PlaySingle(snailHit);
        else
            smInstance.PlaySingle(snailDie);

        if (hitPoints <= 0f)
        {
            gmInstance.numberOfEnemiesKilled += 1;
            gmInstance.RemoveEnemyFromList(this);

            gmInstance.playerExperience += experiencePoints;
            Destroy(gameObject);
            return;
        }

        hitPointsSlider.value = hitPoints;
    }
}
;