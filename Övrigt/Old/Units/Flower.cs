﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Flower : MonoBehaviour {

    public Slider hitPointsSlider;
    public int hitPoints = 4;

    public Image flowerImage;

    void Start()
    {
        GameManager.instance.AddFlowerToList(this);
    }

    public void LosePoint(int snailAttackPoints)
    {
        hitPoints -= snailAttackPoints;

        if (hitPoints <= 0)
        {
            GameManager.instance.RemoveFlowerFromList(this);
            Destroy(gameObject);
            return;
        }    


        flowerImage.transform.localScale -= new Vector3(0.2f, 0.2f, 0);
        hitPointsSlider.value = hitPoints;
    }
}
