﻿using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

    public int columns = 2;
    public int rows = 2;
    public int nrOfEnemies = 1;
    public int nrOfSecondEnemies = 0;
    public int nrOfBosses = 0;
    public int nrOfFlowers = 1;
    public GameObject[] floorTiles;
    public GameObject[] outerWallTiles;
    public GameObject[] outerWallCornerTiles;
    public GameObject[] enemyTiles;
    public GameObject[] enemy2Tiles;
    public GameObject[] bossTiles;
    public GameObject[] flowerTiles;

    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    void InitialiseList()
    {
        gridPositions.Clear();

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if (x != 0 && y != 0)
                    gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                GameObject toInstantiate;
                if (x == -1 && y == -1 || x == -1 && y == rows || x == columns && y == -1 || x == columns && y == rows)
                {
                    toInstantiate = outerWallCornerTiles[Random.Range(0, outerWallCornerTiles.Length)];
                }
                else if (x == -1 || x == columns || y == -1 || y == rows)
                {
                    toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                }
                else
                    toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                GameObject instance =
                    Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                
                instance.transform.SetParent(boardHolder);

                if (x == -1 && y == -1)
                    instance.transform.Rotate(Vector3.forward * 180);

                else if (x == -1 && y > -1 && y <= rows)
                    instance.transform.Rotate(Vector3.forward * 90);

                else if (x > -1 && x < rows-1 && y == -1)
                    instance.transform.Rotate(Vector3.forward * 180);

                else if (x == columns && y >= -1 && y < rows)
                    instance.transform.Rotate(Vector3.forward * -90);
            }
        }
    }

    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        
        Vector3 randomPosition = gridPositions[randomIndex];
        
        gridPositions.RemoveAt(randomIndex);
        
        return randomPosition;
    }
    
    void LayoutObjectAtRandom(GameObject[] tileArray, int objectCount)
    {
        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }
    
    public void SetupScene()
    {
        BoardSetup();
        
        InitialiseList();
        
        LayoutObjectAtRandom(enemyTiles, nrOfEnemies);

        LayoutObjectAtRandom(enemy2Tiles, nrOfSecondEnemies);

        LayoutObjectAtRandom(bossTiles, nrOfBosses);

        LayoutObjectAtRandom(flowerTiles, nrOfFlowers);
    }
}
