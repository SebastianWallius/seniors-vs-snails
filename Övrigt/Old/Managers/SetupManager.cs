﻿using UnityEngine;
using System.Collections;

public class SetupManager : MonoBehaviour {

    public void SetupLevel(int level)
    {
        GameManager.instance.lastLevel = level;

        if (SoundManager.instance.musicSource.isPlaying)
            SoundManager.instance.musicSource.Stop();

        LevelInfo levelInfo = GameManager.instance.levelManager.GetLevelInfo(level);

        SetupBoard(levelInfo.BoardInfo);
        GameManager.instance.InitialiseGame();
    }

    public void SetupGame(int activeLevel, PlayerInfo playerInfo, BoardInfo boardInfo)
    {
        SetupPlayer(activeLevel, playerInfo);
        SetupBoard(boardInfo);
    }

    public void SetupBoard(BoardInfo boardInfo)
    {
        GameManager.instance.boardManager.rows = boardInfo.Rows;
        GameManager.instance.boardManager.columns = boardInfo.Columns;
        GameManager.instance.boardManager.nrOfEnemies = boardInfo.NumberOfEnemies;
        GameManager.instance.boardManager.nrOfSecondEnemies = boardInfo.NumberOfSecondEnemies;
        GameManager.instance.boardManager.nrOfBosses = boardInfo.NumberOfBosses;
        GameManager.instance.boardManager.nrOfFlowers = boardInfo.NumberOfFlowers;
    }

    public void SetupPlayer(int activeLevel, PlayerInfo playerInfo)
    {
        GameManager.instance.facingRight = true;
        GameManager.instance.activeLevel = activeLevel;
        GameManager.instance.playerExperience = playerInfo.Experience;
    }
}
