﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour {
    public Image[] imagesToFade;

    public float fadeInTime;
    public float fadeOutTime;

    public bool isFadingIn;
    public bool isFadingOut;

    private float alpha = 0f;

    void Awake () {
        foreach (Image image in imagesToFade)
        {
            image.canvasRenderer.SetAlpha(0);
        }
        
	}

    void OnGUI()
    {
        foreach (Image image in imagesToFade)
        {
            image.canvasRenderer.SetAlpha(alpha);
        }
    }

    public IEnumerator FadeIn()
    {
        isFadingIn = true;
        float d = 0.3f / fadeInTime;
        while (alpha < 1)
        {
            alpha += Time.deltaTime * d;
            yield return null;
        }
        isFadingIn = false;
    }

    public IEnumerator FadeOut()
    {
        isFadingOut = true;
        float d = 0.3f / fadeOutTime;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime * d;
            yield return null;
        }
        isFadingOut = false;
    }
}
