﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool vibrationActive = true;

    //Initialises GameManager instance to null to achieve singleton behavior
    public static GameManager instance = null;

    //Stack of strings representing scene names, used to keep track of navigation in menus to be able to go back to previous scene
    //Move to MenuManager and make MenuManager singleton?
    public Stack<string> navigationStack = new Stack<string>();

    /*Variables to save Player data
     */
    //Variable to save Player experience.
    public int playerExperience = 0;

    public int playerMoney = 0;

    public int playerLives = 5;
    //Variable to current game level to beat for Player.
    public int playerLevel = 1;

    //Variable to determine length of enemy movement delay.
    public float enemyMovementDelay = 1f;

    public int lastLevel;

    public bool hasPlayedLevel = false;

    /* Initialise variables for controlling elements in game.
     * This is for example player movement, generating a game board and menu navigation.
     */
    //Initialise Player object to be able to control player.
    public Player player;
    //BoardManager to be able to control and generate game board.
    public BoardManager boardManager;
    //Declare MenuManager variable to handle menu navigation.
    private MenuManager menuManager;

    public SetupManager setupManager;

    private ResetManager resetManager;

    public LevelManager levelManager;

    //Number that represents the level that is currently being played.
    public int activeLevel = 0;

    //Bool used when delaying enemy movement.
    public bool enemiesMoving = false;

    //List of all Enemy units, used to issue them move commands.
    public List<Enemy> enemies;
    //List of all flowers, used to move enemies towards flowers and check if any flowers are remaining
    public List<Flower> flowers;

    //Vector used to store location of screen touch origin for mobile controls, initialised to off screen.
    private Vector2 touchOrigin = -Vector2.one;

    //Bool to keep track of which direction 2D Player is facing.
    public bool facingRight = true;

    public int numberOfEnemiesKilled = 0;

    public int numberOfDeaths = 0;

    void Awake()
    {
        //Set instance to this if no instance exists to keep singleton behavior
        if (instance == null)
        {
            instance = this;
        }
        //Destroy current gameObject if instance already exists to only have one instance
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Lets gameObject persist between scenes
        DontDestroyOnLoad(gameObject);

        //Assign enemies to a new List of Enemy objects.
        enemies = new List<Enemy>();
        //Assign flowers to a new List of Flower objects.
        flowers = new List<Flower>();

        //Set menuManager to MenuManager component
        menuManager = GetComponent<MenuManager>();

        //Set setupManager to SetupManager component
        setupManager = GetComponent<SetupManager>();

        //Set setupManager to SetupManager component
        resetManager = GetComponent<ResetManager>();

        //Set boardManager to BoardManager component
        boardManager = GetComponent<BoardManager>();

        //Set levelManager to LevelManager component
        levelManager = GetComponent<LevelManager>();

        //Initialise game if game is started in Unity editor on Level1 scene.
        if (instance.activeLevel == 0 && SceneManager.GetActiveScene().name == "Level1")
        {
            InitialiseGame();
        }
    }

    void OnLevelWasLoaded(int level)
    {
        if (instance != this)
            return;

        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName == "Level1")
        {
            instance.setupManager.SetupLevel(instance.activeLevel);
        }
    }

    //Initialise game by setting up scene and 
    public void InitialiseGame()
    {
        instance.boardManager.SetupScene();
        instance.player = FindObjectOfType<Player>();
    }

    void Update()
    {
        //Move enemies if enemies should move and there are enemies left to move
        if (!enemiesMoving && enemies.Count > 0)
            StartCoroutine(MoveEnemies());
        //Take input if Player exists and is currently not moving
        if (player != null && player.playerState == PlayerState.Idle)
        {
            //Used to store the horizontal move direction.
            int horizontal = 0;
            //Used to store the vertical move direction.
            int vertical = 0;

            //Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
            horizontal = (int)(Input.GetAxisRaw("Horizontal"));

            //Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
            vertical = (int)(Input.GetAxisRaw("Vertical"));

            //Check if moving horizontally, if so set vertical to zero.
            if (horizontal != 0)
            {
                vertical = 0;
            }
            //If not moving horizontally, check if moving vertically. If so set horizontal to zero.
            else if (vertical != 0)
            {
                horizontal = 0;
            }
            
            //Check if Input has registered more than zero touches
            if (Input.touchCount > 0)
            {
                //Store the first touch detected.
                Touch myTouch = Input.touches[0];
                
                //Check if the phase of that touch equals Began
                if (myTouch.phase == TouchPhase.Began)
                {
                    //If so, set touchOrigin to the position of that touch
                    touchOrigin = myTouch.position;
                }

                //If the touch phase is not Began, and instead is equal to Ended and the x of touchOrigin is greater or equal to zero:
                else if (myTouch.phase == TouchPhase.Moved && touchOrigin.x >= 0)
                {
                    //Set touchEnd to equal the position of this touch
                    Vector2 touchEnd = myTouch.position;

                    //Calculate the difference between the beginning and end of the touch on the x axis.
                    float x = touchEnd.x - touchOrigin.x;

                    //Calculate the difference between the beginning and end of the touch on the y axis.
                    float y = touchEnd.y - touchOrigin.y;

                    //Set touchOrigin.x to -1 so that our else if statement will evaluate false and not repeat immediately.
                    touchOrigin.x = -1;

                    //Check if the difference along the x axis is greater than the difference along the y axis.
                    if (Mathf.Abs(x) > Mathf.Abs(y))
                        //If x is greater than zero, set horizontal to 1, otherwise set it to -1
                        horizontal = x > 0 ? 1 : -1;
                    else
                        //If y is greater than zero, set horizontal to 1, otherwise set it to -1
                        vertical = y > 0 ? 1 : -1;
                }
            }

            //If input is received, reset input and start player movement
            if (horizontal != 0 || vertical != 0)
            {

                if (facingRight && horizontal == -1 || !facingRight && horizontal == 1)
                {
                    facingRight = horizontal == 1;
                    player.transform.localScale = new Vector2(-player.transform.localScale.x, player.transform.localScale.y);
                }
                    

                Input.ResetInputAxes();
                StartCoroutine(MovePlayer(horizontal, vertical));
            }
        }
    }

    //Moves Player and stops taking input until movement is finished
    IEnumerator MovePlayer(int horizontal, int vertical)
    {
        //Save start position to be able to calculate end position
        player.startPosition = player.transform.position;
        //Attempt to move Player
        player.AttemptMove<Enemy>(horizontal, vertical);
        //Delay accepting user input until movement is finished
        while (player != null && player.transform.position != player.startPosition + new Vector3(horizontal, vertical, 0f))
        {
            yield return null;
        }
        //Register that Player has finished moving
        player.playerState = PlayerState.Idle;
    }

    //Coroutine to move enemies in sequence.
    IEnumerator MoveEnemies()
    {
        //While enemiesMoving is true player is unable to move.
        enemiesMoving = true;

        //Wait for turnDelay seconds, defaults to .1 (100 ms).
        //yield return new WaitForSeconds(turnDelay);

        int flowerIndex;

        //If there are no enemies spawned:
        if (enemies.Count == 0)
        {
            //Wait for turnDelay seconds between moves, replaces delay caused by enemies moving when there are none.
            yield return new WaitForSeconds(enemyMovementDelay);
        }

        //Loop through List of Enemy objects.
        for (int i = 0; i < instance.enemies.Count; i++)
        {
            if (flowers.Count == 0)
                break;
            //Check if either flower or enemy at current position is null before attempting movement
            if (i <= flowers.Count-1 && flowers[i] == null)
            {
                RemoveFlowerFromList(flowers[i]);
                break;
            }
            if (enemies[i] == null)
            {
                RemoveEnemyFromList(enemies[i]);
                break;
            }

            flowerIndex = flowers.Count - 1 >= i ? i : flowers.Count - 1;
                
            //Call the MoveEnemy function of Enemy at index i in the enemies List and move towards corresponding flower.
            if (!enemies[i].isMoving)
                enemies[i].MoveEnemy(flowers[flowerIndex].transform);

            //Wait for Enemy's moveTime before moving next Enemy, 
            yield return new WaitForSeconds(enemyMovementDelay);
        }

        yield return new WaitForSeconds(enemyMovementDelay);

        //Enemies are done moving, set enemiesMoving to false.
        enemiesMoving = false;
    }

    //Call this to add the passed in Enemy to the List of Enemy objects.
    public void AddEnemyToList(Enemy enemy)
    {
        //Add Enemy to List enemies.
        instance.enemies.Add(enemy);
    }

    //Removes Enemy from enemies array to be able to use Count method
    public void RemoveEnemyFromList(Enemy enemy)
    {
        instance.enemies.Remove(enemy);
    }

    //Returns current Enemy count in enemies array
    public int CountEnemies()
    {
        return instance.enemies.Count;
    }

    //Call this to add the passed in Flower to the List of Flower objects.
    public void AddFlowerToList(Flower flower)
    {
        //Add Enemy to List enemies.
        instance.flowers.Add(flower);
    }

    //Removed Flower from flowers array to be able to use Count method
    public void RemoveFlowerFromList(Flower flower)
    {
        instance.flowers.Remove(flower);
    }

    //Returns current Flower count in flowers array
    public int CountFlowers()
    {
        return instance.flowers.Count;
    }

    public int GetActiveLevel()
    {
        return instance.activeLevel;
    }

    public void SetActiveLevel(int level)
    {
        instance.activeLevel = level;
    }

    //Change to next level
    public void LevelCleared()
    {
        instance.resetManager.ResetGameManager();
        if (instance.lastLevel == instance.playerLevel)
            instance.playerLevel += 1;
        instance.hasPlayedLevel = true;

        LevelInfo levelInfo = instance.levelManager.GetLevelInfo(instance.lastLevel);
        if (!levelInfo.IsFinished)
            instance.playerMoney += levelInfo.MoneyReward;

        levelInfo.IsFinished = true;
        menuManager.LoadPreviousScene();
    }

    //Method to run when user did not finish current level
    public void GameOver()
    {
        instance.numberOfDeaths += 1;
        enemiesMoving = false;
        StopAllCoroutines();
        //Stop removing lives at 1 to avoid player playing 0 or negative lives until lives are fully implemented
        if (instance.playerLives > 1)
            instance.playerLives -= 1;
        instance.resetManager.ResetGameManager();
        instance.hasPlayedLevel = true;
        menuManager.LoadPreviousScene();
    }
}
