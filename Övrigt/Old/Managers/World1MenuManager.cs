﻿using UnityEngine;
using System.Collections;

public class World1MenuManager : MonoBehaviour {

    public LevelClearedMenu levelClearedMenu;
    public GameOverMenu gameOverMenu;

    void Start()
    {
        GameManager gmInstance = GameManager.instance;

        if (gmInstance.hasPlayedLevel)
        {
            gmInstance.hasPlayedLevel = false;
            levelClearedMenu.ToggleLevelClearedCanvas(gmInstance.levelManager.GetLevelInfo(gmInstance.lastLevel).IsFinished);
            gameOverMenu.SetGameMenuVisibility(!gmInstance.levelManager.GetLevelInfo(gmInstance.lastLevel).IsFinished);
        }
    }
}
