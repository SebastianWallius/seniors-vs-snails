﻿using UnityEngine;
using System.Collections;

public class ResetManager : MonoBehaviour {

    //For potential future use
    /*public void ResetGame()
    {
        BoardInfo resetGameBoardInfo = new BoardInfo(3, 3, 1, 0);
        PlayerInfo resetGamePlayerInfo = new PlayerInfo(0, 0);
        GameManager.instance.setupManager.SetupGame(1, resetGamePlayerInfo, resetGameBoardInfo);
    }*/

    public void ResetGameManager()
    {
        GameManager.instance.activeLevel = 0;
        GameManager.instance.player.playerState = PlayerState.Idle;
        GameManager.instance.enemiesMoving = false;
        GameManager.instance.enemies.Clear();
        GameManager.instance.flowers.Clear();

        //SoundManager.instance.sfxSource.Stop();
        //SoundManager.instance.musicSource.Play();
    }

}
