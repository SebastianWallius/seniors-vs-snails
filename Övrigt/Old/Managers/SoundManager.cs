﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
    public AudioSource sfxSource;
    public AudioSource musicSource;

    public static SoundManager instance = null;


	void Awake () {
        //Set instance to this if no instance exists to keep singleton behavior
        if (instance == null)
        {
            instance = this;
        }
        //Destroy current gameObject if instance already exists to only have one instance
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Lets gameObject persist between scenes
        DontDestroyOnLoad(gameObject);
    }
	
    public void PlaySingle(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }

    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);

        sfxSource.clip = clips[randomIndex];
        sfxSource.Play();
    }
}
