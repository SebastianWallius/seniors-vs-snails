﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public void LoadNextScene(string sceneToLoad)
    {
        if (string.IsNullOrEmpty(sceneToLoad))
            return;
        GameManager.instance.navigationStack.Push(SceneManager.GetActiveScene().name);
        SceneManager.LoadScene(sceneToLoad);
    }

    public void LoadPreviousScene()
    {
        if (GameManager.instance.navigationStack.Count == 0)
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "Level1":
                    SceneManager.LoadScene("World1");
                    break;
                default:
                    return;
            }
            
        }
        else
            SceneManager.LoadScene(GameManager.instance.navigationStack.Pop());
    }
}
