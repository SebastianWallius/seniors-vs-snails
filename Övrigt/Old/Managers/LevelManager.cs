﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

    public List<LevelInfo> levelInformations = null;

    void Awake()
    {
        //Add 10 levels:
        if (levelInformations != null)
            return;

        levelInformations = new List<LevelInfo>();
        levelInformations.Add(new LevelInfo(1, 4, 3, 1, 0, 0, 0));
        levelInformations.Add(new LevelInfo(2, 4, 3, 1, 0, 0, 1));
        levelInformations.Add(new LevelInfo(3, 4, 3, 2, 0, 0, 2));
        levelInformations.Add(new LevelInfo(4, 4, 3, 2, 0, 0, 1));
        levelInformations.Add(new LevelInfo(5, 4, 3, 0, 1, 0, 1));
        levelInformations.Add(new LevelInfo(6, 4, 3, 1, 1, 0, 2));
        levelInformations.Add(new LevelInfo(7, 4, 3, 1, 1, 0, 1));
        levelInformations.Add(new LevelInfo(8, 4, 3, 0, 2, 0, 2));
        levelInformations.Add(new LevelInfo(9, 4, 3, 0, 2, 0, 1));
        levelInformations.Add(new LevelInfo(10, 4, 3, 0, 0, 1, 2));
    }

    public LevelInfo GetLevelInfo(int level)
    {
        if (levelInformations.Count < level)
            return null;

        return levelInformations[level - 1];
    }
}
